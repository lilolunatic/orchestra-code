from django.contrib.auth.models import User
from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import filters
from knox.models import AuthToken

from .serializers import UserSerializer, RegisterSerializer, LoginSerializer, ProfileSerializer
from modules.models import Instrument
from django.http import JsonResponse


class UserListView(generics.ListAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['username']
    # page size is defined in the settings


class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })


# Login API
class LoginAPI(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })


# Get User API
class UserAPI(generics.RetrieveAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user


# Get Profile
class ProfileViewSet(APIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    http_method_names = ['get', 'head', 'post']

    serializer_class = ProfileSerializer

    def get(self, request):
        user = self.request.user

        allInstruments = []
        userInstruments = []

        for i_N in Instrument.objects.all().values("instrument"):
            allInstruments.append(i_N["instrument"])

        for u_I in Instrument.objects.filter(profile=user.profile).values("instrument"):
            userInstruments.append(u_I["instrument"])

        instrumentDict = {}

        for instrumentName in allInstruments:
            if instrumentName in userInstruments:
                instrumentDict.update({instrumentName: True})
            else:
                instrumentDict.update({instrumentName: False})

        if user.profile.image:
            return JsonResponse({
                "user": [user.id, user.username],
                # {"flute": True, "piano":False, "trumpet": True}
                "instruments": instrumentDict,
                "profileImage": user.profile.image,
            })
        else:
            return JsonResponse({
                "user": [user.id, user.username],
                # {"flute": True, "piano":False, "trumpet": True}
                "instruments": instrumentDict,
                "profileImage": None,
            })

    def post(self, request):

        """
        data in request: {"name": ["instrument_name", ""instrument_name2]}
        get a list with instruments where a user play
        update the database after this information
        :param request:
        :return:
        """

        instrument_names = self.request.data["name"]
        new_player_instruments = set(
            Instrument.objects.filter(instrument__in=instrument_names))

        current_player_instruments = set(
            Instrument.objects.filter(profile=self.request.user.profile))

        instruments_to_add = new_player_instruments.difference(
            current_player_instruments)
        for instrument in instruments_to_add:
            self.request.user.profile.instruments.add(instrument)

        instruments_to_remove = current_player_instruments.difference(
            new_player_instruments)
        for instrument in instruments_to_remove:
            self.request.user.profile.del_instrument(instrument)

        return JsonResponse({"successful": True})
