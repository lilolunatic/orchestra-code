import json

from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIClient
from modules.models import Instrument, UserGroupRole, Profile, Group
from orchestra.settings import REST_FRAMEWORK


class PostManageInstrumentsTestCase(TestCase):
    def setUp(self):
        self.flute = Instrument.objects.create(instrument="flute")
        Instrument.objects.create(instrument="violin")

        self.user1 = User.objects.create(username="TestUser1", password=1234)

        self.group = Group.objects.create(group_name="test-group")
        self.group_user = UserGroupRole.objects.create(role='mu', profile=Profile.objects.get(user=self.user1),
                                                       group=self.group)

        client = APIClient()
        client.force_authenticate(user=self.user1)

    def test_post_request(self):
        client = APIClient()
        client.force_authenticate(user=self.user1)

        request_1 = client.post('/api/profile',
                                data={"name": ["flute", "violin"]},
                                format='json')
        self.assertEqual(request_1.status_code, 200)
        self.assertListEqual(list(Instrument.objects.filter(profile=self.user1.profile)),
                             list(Instrument.objects.filter(instrument__in=["flute", "violin"])))

        # Case2: Uer play flute, should now do not play anymore flute
        request_2 = client.post('/api/profile',
                                data={"name": ["flute"]},
                                format='json')
        self.assertEqual(request_2.status_code, 200)
        self.assertListEqual(list(Instrument.objects.filter(profile=self.user1.profile)),
                             list(Instrument.objects.filter(instrument__in=["flute"])))

        self.group_user.instruments.add(self.flute)
        request_2 = client.post('/api/profile',
                                data={"name": []},
                                format='json')
        self.assertEqual(request_2.status_code, 200)
        self.assertListEqual(list(Instrument.objects.filter(profile=self.user1.profile)),
                             list(Instrument.objects.filter(instrument__in=[])))

        # test if the instrument from an UserGroupRole also get deleted
        self.assertListEqual(list(Instrument.objects.filter(usergrouprole=self.group_user)),
                             list(Instrument.objects.filter(instrument__in=[])))


class SearchUserTestCase(TestCase):

    def setUp(self) -> None:
        user1 = User.objects.create(username="TestUser0")

        self.client = APIClient()
        self.client.force_authenticate(user=user1)

        for i in range(1, 21):
            User.objects.create(username=f"TestUser{i}")

    def test_x_user(self):
        response = self.client.get('/api/user?search=TestUser1')
        json_data = json.loads(response.json.args[0].content.decode('utf8').replace("'", '"'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(json_data["count"], 11)
        self.assertTrue(len(json_data["results"]) <= REST_FRAMEWORK["PAGE_SIZE"])

    def test_one_user(self):
        username = "TestUser14"
        response = self.client.get(f'/api/user?search={username}')
        json_data = json.loads(response.json.args[0].content.decode('utf8').replace("'", '"'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(json_data["count"], 1)
        self.assertDictEqual(json_data['results'][0],
                             {'id': User.objects.get(username=username).id, 'username': username, 'email': ''})
