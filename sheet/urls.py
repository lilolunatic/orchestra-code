from django.urls import path

from sheet.api import SheetApi

urlpatterns = [
    path('api/sheet/<int:scoreId>', SheetApi.as_view()),
    path('api/sheet', SheetApi.as_view()),

]