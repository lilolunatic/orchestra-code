
from modules.models import Sheet, Instrument


def sheet_creator(sheets, score_id):
    sheets = [Sheet.objects.create(score_id=score_id,
                                   name=sheet["name"],
                                   instrument=Instrument.objects.get(instrument=sheet["instrument"]),
                                   file=sheet["file"])
              for sheet in sheets]

    sheets = [{"instrument": sheet.instrument.instrument,
               "name": sheet.name, "id": sheet.id} for sheet in sheets]

    return sheets
