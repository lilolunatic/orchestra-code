import json

from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIClient

from modules.models import Group, Score, Sheet, Instrument


class ScoreTestCase(TestCase):
    def setUp(self):
        self.group = Group.objects.create(group_name="Gruppe")
        self.group2 = Group.objects.create(group_name="Gruppe2")

        self.violin = Instrument.objects.create(instrument="Violin")

        self.score = Score.objects.create(name="Test Score", group_id=self.group.id)
        self.sheet = Sheet.objects.create(name="test sheet", score=self.score, instrument=self.violin)

        self.user = User.objects.create(username="Test", password="1234")
        self.user.profile.instruments.add(self.violin)
        self.client = APIClient()
        self.client.force_authenticate(self.user)

    def test_post_request(self):
        sheet_counter = Sheet.objects.all().count()
        sheet_name = "sheet1"
        response = self.client.post("/api/sheet",
                                    data={"scoreId": self.score.id,
                                          "sheets": [{
                                              "name": sheet_name,
                                              "instrument": self.violin.instrument,
                                              "file": "daten oder so"
                                          }]},
                                    format='json')

        expected = {
            "sheets": [{
                "name": sheet_name,
                "id": sheet_counter + 1,
                "instrument": self.sheet.instrument.instrument,
            }]
        }
        json_data = response.json.args[0].content.decode('utf8').replace("'", '"')
        json_data = json.loads(json_data)

        self.assertDictEqual(expected, json_data)
