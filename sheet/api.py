from django.http import JsonResponse, FileResponse
from rest_framework import permissions
from rest_framework.views import APIView
from sheet.sheet_helpers import sheet_creator

from modules.models import Sheet, Instrument


class SheetApi(APIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    http_method_names = ['head', 'post']

    def post(self, request):
        data = self.request.data

        return JsonResponse({"sheets": sheet_creator(data["sheets"], data["scoreId"])})
