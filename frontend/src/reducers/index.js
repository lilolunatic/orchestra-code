import { combineReducers } from "redux";
import { LOGOUT } from "../actions/types";
import auth from "./auth";
import navigation from "./navigation";
import profile from "./profile";
import groups from "./groups";
import score from "./score";
import groupManager from "./groupManager";

const appReducer = combineReducers({
  auth,
  navigation,
  profile,
  groups,
  score,
  groupManager,
});

const rootReducer = (state, action) => {
  if (action.type === LOGOUT) {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
