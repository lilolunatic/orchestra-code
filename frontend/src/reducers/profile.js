import {
  LOAD_PROFILE_DATA,
  LOAD_PROFILE_DATA_SUCCESS,
  LOAD_PROFILE_DATA_FAILURE,
  SET_USER_INSTRUMENT,
  SET_USER_INSTRUMENT_SUCCESS,
  SET_USER_INSTRUMENT_FAILURE,
  SET_PROFILE_IMAGE,
  SET_PROFILE_IMAGE_SUCCESS,
} from "../actions/types";

export const initialState = {
  username: "",
  userid: undefined,
  instrumentDict: {},
  isLoading: false,
  isError: false,
  profileImage: "",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOAD_PROFILE_DATA:
    case SET_USER_INSTRUMENT:
    case SET_PROFILE_IMAGE:
      return {
        ...state,
        isLoading: true,
      };

    case LOAD_PROFILE_DATA_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        userid: action.payload.user[0],
        username: action.payload.user[1],
        instrumentDict: action.payload.instruments,
        profileImage: action.payload.profileImage
          ? action.payload.profileImage
          : "/media/default/default.png",
      };

    case LOAD_PROFILE_DATA_FAILURE:
      return {
        isLoading: false,
        isError: true,
      };

    case SET_USER_INSTRUMENT_SUCCESS:
      let changedInstruments = action.payload.instruments;
      let instrumentDictChanged = state.instrumentDict;

      Object.keys(instrumentDictChanged).map((i) => {
        if (changedInstruments.includes(i)) {
          return (instrumentDictChanged[i] = true);
        }
        return (instrumentDictChanged[i] = false);
      });

      return {
        ...state,
        isLoading: false,
        isError: false,
        instrumentDict: instrumentDictChanged,
      };

    case SET_USER_INSTRUMENT_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };

    case SET_PROFILE_IMAGE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        profileImage: action.payload.profileImage,
      };

    default:
      return state;
  }
}
