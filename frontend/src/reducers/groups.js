import {
  LOAD_USER_GROUPS,
  LOAD_USER_GROUPS_SUCCESS,
  LOAD_USER_GROUPS_FAILURE,
  LOAD_SPECIFIC_GROUP,
  LOAD_SPECIFIC_GROUP_FAILURE,
  LOAD_SPECIFIC_GROUP_SUCCESS,
  SET_USER_GROUP_INSTRUMENTS,
  SET_USER_GROUP_INSTRUMENTS_FAILURE,
  SET_USER_GROUP_INSTRUMENTS_SUCCESS,
  INIT_GROUP_MANAGE_STATUS,
  SET_GROUP_ID,
  SET_USER_ROLE,
  CLEAR_SPECIFIC_GROUP,
} from "../actions/types";

export const initialState = {
  groupId: "blu",
  userGroups: {},
  userName: "",
  isLoading: false,
  isError: false,
  groupName: "",
  members: [],
  groupInstruments: [],
  userGroupInstruments: [],
  manageStatus: "",
  groupScores: [],
  userRole: "",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_USER_GROUP_INSTRUMENTS:
    case LOAD_USER_GROUPS:
      return {
        ...state,
        isLoading: true,
      };

    case LOAD_USER_GROUPS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        userGroups: action.payload.groups,
      };

    case LOAD_SPECIFIC_GROUP:
      return {
        ...state,
        isLoading: true,
        userName: action.payload,
        userRole: "",
        members: [],
      };

    case LOAD_SPECIFIC_GROUP_SUCCESS:
      let instruments = [];
      action.payload.members.forEach((m) => {
        m.instruments.forEach((i) => {
          if (!instruments.includes(i) && i !== null) {
            instruments.push(i);
          }
        });
      });

      let userGrpInstNull = action.payload.members.filter(
        (m) => m.name === state.userName
      )[0].instruments;

      let userGrpInst = userGrpInstNull.filter((e) => {
        return e !== null;
      });

      return {
        ...state,
        isLoading: false,
        isError: false,
        groupName: action.payload.groupName,
        members: action.payload.members,
        groupInstruments: instruments,
        userGroupInstruments: userGrpInst,
        groupScores: action.payload.scores,
      };

    case SET_USER_GROUP_INSTRUMENTS_FAILURE:
    case LOAD_SPECIFIC_GROUP_FAILURE:
    case LOAD_USER_GROUPS_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };

    case SET_USER_GROUP_INSTRUMENTS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        userGroupInstruments: action.payload,
      };

    case INIT_GROUP_MANAGE_STATUS:
      return {
        ...state,
        manageStatus: action.payload,
      };

    case SET_GROUP_ID:
      return {
        ...state,
        groupId: action.payload,
      };

    case SET_USER_ROLE:
      return {
        ...state,
        userRole: action.payload,
      };

    case CLEAR_SPECIFIC_GROUP:
      return {
        ...state,
        groupId: "",
        groupName: "",
        members: [],
        groupInstruments: [],
        userGroupInstruments: [],
        groupScores: [],
        userRole: "",
      };

    default:
      return {
        ...state,
      };
  }
}
