import reducer, { initialState } from "../groupManager";
import {
  LOAD_USER_SUGGESTIONS,
  LOAD_USER_SUGGESTIONS_SUCCESS,
  LOAD_USER_SUGGESTIONS_FAILURE,
  CREATE_GROUP,
  CREATE_GROUP_SUCCESS,
  CREATE_GROUP_FAILURE,
  RESET_AFTER_SAVE_GROUP,
  EDIT_GROUP,
  EDIT_GROUP_SUCCESS,
  EDIT_GROUP_FAILURE,
} from "../../actions/types";

const fakeGroupId = 12;
const fakeSearchResults = {
  results: [
    { id: 1, username: "anna" },
    { id: 2, username: "anton" },
  ],
};
const fakeUsers = [
  { id: 1, username: "anna" },
  { id: 2, username: "anton" },
];

describe("groupManager reducer tests", () => {
  it("Save group data test", () => {
    const stateBefore = initialState;
    const action = { type: CREATE_GROUP };
    const stateAfter = {
      ...initialState,
      isLoading: true,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Save group data successful", () => {
    const stateBefore = initialState;
    const action = {
      type: CREATE_GROUP_SUCCESS,
      payload: fakeGroupId,
    };
    const stateAfter = {
      ...initialState,
      isLoading: false,
      isError: false,
      groupId: fakeGroupId,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Save group data fails", () => {
    const stateBefore = initialState;
    const action = { type: CREATE_GROUP_FAILURE };
    const stateAfter = {
      ...initialState,
      isLoading: false,
      isError: true,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("reset groupId after group creation success", () => {
    const stateBefore = initialState;
    const action = { type: RESET_AFTER_SAVE_GROUP };
    const stateAfter = {
      ...initialState,
      isLoading: false,
      groupId: null,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Load user suggestions", () => {
    const stateBefore = initialState;
    const action = { type: LOAD_USER_SUGGESTIONS };
    const stateAfter = {
      ...initialState,
      isLoading: true,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Load User Suggestions successful", () => {
    const stateBefore = initialState;
    const action = {
      type: LOAD_USER_SUGGESTIONS_SUCCESS,
      payload: fakeSearchResults,
    };
    const stateAfter = {
      ...initialState,
      isLoading: false,
      isError: false,
      searchResults: fakeUsers,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Load User Suggestions fails", () => {
    const stateBefore = initialState;
    const action = { type: LOAD_USER_SUGGESTIONS_FAILURE };
    const stateAfter = {
      ...initialState,
      isLoading: false,
      isError: true,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Edit group data test", () => {
    const stateBefore = initialState;
    const action = { type: EDIT_GROUP };
    const stateAfter = {
      ...initialState,
      isLoading: true,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Edit group data successful", () => {
    const stateBefore = initialState;
    const action = {
      type: EDIT_GROUP_SUCCESS,
    };
    const stateAfter = {
      ...initialState,
      isLoading: false,
      isError: false,
      editGroupSuccess: true,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Edit group data fails", () => {
    const stateBefore = initialState;
    const action = { type: EDIT_GROUP_FAILURE };
    const stateAfter = {
      ...initialState,
      isLoading: false,
      isError: true,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });
});
