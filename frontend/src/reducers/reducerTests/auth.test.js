import {
  USER_LOADING,
  USER_LOADED,
  LOGIN_SUCCESS,
  AUTH_ERROR,
} from "../../actions/types";
import reducer, { initialState } from "../auth";

const fakeUser = "Max Mustermann";
const fakeToken = "1234abcd";

describe("Authorization Reducer Test", () => {
  it("User loading", () => {
    const stateBefore = initialState;
    const action = { type: USER_LOADING };
    const stateAfter = {
      ...initialState,
      isLoading: true,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("User loaded with success", () => {
    const stateBefore = initialState;
    const action = {
      type: USER_LOADED,
      payload: {
        user: fakeUser,
      },
    };
    const stateAfter = {
      ...initialState,
      isAuthenticated: true,
      isLoading: false,
      user: { user: fakeUser },
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Login was successful", () => {
    const stateBefore = initialState;
    const resp = { user: fakeUser, token: fakeToken };
    const action = {
      type: LOGIN_SUCCESS,
      payload: resp,
    };

    const stateAfter = {
      isAuthenticated: true,
      isLoading: false,
      isError: false,
      user: fakeUser,
      token: fakeToken,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Auth error", () => {
    const stateBefore = initialState;
    const action = {
      type: AUTH_ERROR,
    };

    const stateAfter = {
      isAuthenticated: false,
      isError: true,
      isLoading: false,
      user: null,
      token: null,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("default behavior", () => {
    const stateBefore = initialState;
    const action = {};

    const stateAfter = {
      token: null,
      isAuthenticated: null,
      isError: false,
      isLoading: false,
      user: null,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });
});
