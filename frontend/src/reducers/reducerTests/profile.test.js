import { describe, expect, it } from "@jest/globals";
import {
  LOAD_PROFILE_DATA,
  LOAD_PROFILE_DATA_SUCCESS,
  LOAD_PROFILE_DATA_FAILURE,
  SET_USER_INSTRUMENT,
  SET_USER_INSTRUMENT_SUCCESS,
  SET_PROFILE_IMAGE_SUCCESS,
} from "../../actions/types";
import reducer, { initialState } from "../profile";

const fakeUserId = 1;
const fakeUsername = "Johann Sebastian Bach";
const fakeInstrumentDict = {
  organ: true,
  "e-guitar": false,
  violin: false,
  marimba: true,
};
const marimbatoRemove = ["organ"];
const violinToAdd = ["organ", "violin", "marimba"];
const removedInstrumentDict = {
  organ: true,
  "e-guitar": false,
  violin: false,
  marimba: false,
};
const addInstrumentDict = {
  organ: true,
  "e-guitar": false,
  violin: true,
  marimba: true,
};
const fakeProfilePic = "ABCDATAURL";

describe("Profile Data Reducer Test", () => {
  it("load profile Data", () => {
    const stateBefore = initialState;
    const action = { type: LOAD_PROFILE_DATA };
    const stateAfter = {
      ...initialState,
      isLoading: true,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("load profile Data Success", () => {
    const stateBefore = initialState;
    const action = {
      type: LOAD_PROFILE_DATA_SUCCESS,
      payload: {
        user: [fakeUserId, fakeUsername],
        instruments: fakeInstrumentDict,
        profileImage: fakeProfilePic,
      },
    };
    const stateAfter = {
      ...initialState,
      isLoading: false,
      isError: false,
      userid: fakeUserId,
      username: fakeUsername,
      instrumentDict: fakeInstrumentDict,
      profileImage: fakeProfilePic,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("load profile data failure", () => {
    const stateBefore = initialState;
    const action = { type: LOAD_PROFILE_DATA_FAILURE };
    const stateAfter = {
      isError: true,
      isLoading: false,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });
});

describe("Change Instrument Reducer Test", () => {
  it("set user instrument", () => {
    const stateBefore = initialState;
    const action = { type: SET_USER_INSTRUMENT };
    const stateAfter = {
      ...initialState,
      isLoading: true,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("set user instrument success: remove marimba", () => {
    const stateBefore = {
      ...initialState,
      instrumentDict: fakeInstrumentDict,
    };
    const action = {
      type: SET_USER_INSTRUMENT_SUCCESS,
      payload: {
        instruments: marimbatoRemove,
      },
    };
    const stateAfter = {
      ...initialState,
      isLoading: false,
      isError: false,
      instrumentDict: removedInstrumentDict,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("set user instrument success: add violin", () => {
    const stateBefore = {
      ...initialState,
      instrumentDict: fakeInstrumentDict,
    };
    const action = {
      type: SET_USER_INSTRUMENT_SUCCESS,
      payload: {
        instruments: violinToAdd,
      },
    };
    const stateAfter = {
      ...initialState,
      isLoading: false,
      isError: false,
      instrumentDict: addInstrumentDict,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("add profile image", () => {
    const stateBefore = { ...initialState, isLoading: true };
    const action = {
      type: SET_PROFILE_IMAGE_SUCCESS,
      payload: {
        profileImage: fakeProfilePic,
      },
    };

    const stateAfter = {
      ...initialState,
      isLoading: false,
      isError: false,
      profileImage: fakeProfilePic,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });
});
