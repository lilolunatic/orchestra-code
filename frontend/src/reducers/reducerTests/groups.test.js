import reducer, { initialState } from "../groups";
import {
  LOAD_USER_GROUPS,
  LOAD_USER_GROUPS_SUCCESS,
  LOAD_SPECIFIC_GROUP,
  LOAD_SPECIFIC_GROUP_SUCCESS,
  LOAD_USER_GROUPS_FAILURE,
  SET_USER_ROLE,
  INIT_GROUP_MANAGE_STATUS,
  SET_GROUP_ID,
  CLEAR_SPECIFIC_GROUP,
} from "../../actions/types";

const fakeUserGroups = [
  { id: 1, name: "FirstFakeGroup" },
  { id: 2, name: "SecondFakeGroup" },
];
const fakeUserName = "ErikSatie";
const members = [
  {
    id: 1,
    name: "ErikSatie",
    role: "co",
    instruments: ["piano", "flute"],
    image: "ABC",
  },
  {
    id: 2,
    name: "Gnosiennes",
    role: "mu",
    instruments: ["piano", "accordeon"],
    profileImage: "DEF",
  },
];
const scores = [
  { name: "ABC", id: 0, instrument: "flute" },
  { name: "DEF", id: 1, instrument: "accordeon" },
];
const fakeGroup = {
  groupName: "FakeGroup",
  members,
  scores,
};

const fakeGroupId = 4;

const manageStatusBefore = "edit";

const manageStatusAfter = "create";

describe("Group Data Reducer Tests", () => {
  it("Loading data test", () => {
    const stateBefore = initialState;
    const action = { type: LOAD_USER_GROUPS };
    const stateAfter = {
      ...initialState,
      isLoading: true,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Loading user groups successful", () => {
    const stateBefore = initialState;
    const action = {
      type: LOAD_USER_GROUPS_SUCCESS,
      payload: { groups: fakeUserGroups },
    };
    const stateAfter = {
      ...initialState,
      isLoading: false,
      isError: false,
      userGroups: fakeUserGroups,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Loading specific user group", () => {
    const stateBefore = initialState;
    const action = { type: LOAD_SPECIFIC_GROUP, payload: fakeUserName };
    const stateAfter = {
      ...initialState,
      isLoading: true,
      userName: fakeUserName,
      members: [],
      userRole: "",
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Loading specific user group successful", () => {
    const stateBefore = {
      ...initialState,
      userName: fakeUserName,
    };
    const action = {
      type: LOAD_SPECIFIC_GROUP_SUCCESS,
      payload: fakeGroup,
    };
    const stateAfter = {
      ...initialState,
      isLoading: false,
      isError: false,
      userName: fakeUserName,
      groupName: "FakeGroup",
      members: members,
      groupInstruments: ["piano", "flute", "accordeon"],
      userGroupInstruments: ["piano", "flute"],
      groupScores: scores,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Loading of group data fails", () => {
    const stateBefore = initialState;
    const action = { type: LOAD_USER_GROUPS_FAILURE };
    const stateAfter = {
      ...initialState,
      isLoading: false,
      isError: true,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("set role of user in group", () => {
    const stateBefore = initialState;
    const action = {
      type: SET_USER_ROLE,
      payload: 1,
    };
    const stateAfter = {
      ...initialState,
      userRole: 1,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Clear group data from store", () => {
    const stateBefore = {
      ...initialState,
      userName: fakeUserName,
      userRole: "ma",
      groupId: fakeGroupId,
      groupName: "FakeGroup",
      members: members,
      groupInstruments: ["piano", "flute", "accordeon"],
      userGroupInstruments: ["piano", "flute"],
      groupScores: scores,
    };
    const action = {
      type: CLEAR_SPECIFIC_GROUP,
    };
    const stateAfter = {
      ...initialState,
      userName: fakeUserName,
      userRole: "",
      groupId: "",
      groupName: "",
      members: [],
      groupInstruments: [],
      userGroupInstruments: [],
      groupScores: [],
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("Set initial groupManager state", () => {
    const stateBefore = {
      ...initialState,
      manageStatus: manageStatusBefore,
    };
    const action = {
      type: INIT_GROUP_MANAGE_STATUS,
      payload: manageStatusAfter,
    };
    const stateAfter = {
      ...initialState,
      manageStatus: manageStatusAfter,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it("set group id", () => {
    const stateBefore = {
      ...initialState,
      groupId: 2,
    };
    const action = {
      type: SET_GROUP_ID,
      payload: 1,
    };
    const stateAfter = {
      ...initialState,
      groupId: 1,
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });
});
