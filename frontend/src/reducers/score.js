import {
  GET_SCORE_ID,
  GET_SCORE_ID_SUCCESS,
  GET_SCORE_ID_FAILURE,
  CREATE_SCORE,
  CREATE_SCORE_SUCCESS,
  CREATE_SCORE_FAILURE,
  RESET_CREATED,
  LOAD_SCORE,
  LOAD_SCORE_SUCCESS,
  LOAD_SCORE_FAILURE,
} from "../actions/types";

const initialState = {
  created: false,
  scoreId: null,
  isLoading: false,
  isError: false,
  scoreName: "",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CREATE_SCORE:
    case GET_SCORE_ID:
      return {
        ...state,
        isLoading: true,
      };

    case GET_SCORE_ID_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
      };

    case CREATE_SCORE_SUCCESS:
      return {
        ...state,
        scoreName: action.payload.scoreName,
        scoreId: action.payload.scoreId,
        isLoading: false,
        isError: false,
        sheets: action.payload.sheets,
        created: true,
      };

    case LOAD_SCORE:
      return {
        ...state,
        isLoading: true,
        scoreId: action.payload.id,
        scoreName: action.payload.scoreName,
      };

    case LOAD_SCORE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        sheets: action.payload.sheets,
      };

    case LOAD_SCORE_FAILURE:
    case CREATE_SCORE_FAILURE:
    case GET_SCORE_ID_FAILURE:
      return {
        ...state,
        isError: true,
        isLoading: false,
      };

    case RESET_CREATED:
      return {
        ...state,
        created: false,
      };
    default:
      return {
        ...state,
      };
  }
}
