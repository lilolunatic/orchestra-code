import {
  LOAD_USER_SUGGESTIONS,
  LOAD_USER_SUGGESTIONS_SUCCESS,
  LOAD_USER_SUGGESTIONS_FAILURE,
  CREATE_GROUP,
  CREATE_GROUP_SUCCESS,
  CREATE_GROUP_FAILURE,
  EDIT_GROUP,
  EDIT_GROUP_SUCCESS,
  EDIT_GROUP_FAILURE,
  RESET_AFTER_SAVE_GROUP,
} from "../actions/types";

export const initialState = {
  groupName: "",
  groupId: null,
  searchResults: [],
  isLoading: false,
  isError: false,
  savedGroupLoaded: false,
  editGroupSuccess: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CREATE_GROUP:
      return {
        ...state,
        isLoading: true,
      };

    case CREATE_GROUP_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        groupId: action.payload,
      };

    case CREATE_GROUP_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };

    case LOAD_USER_SUGGESTIONS:
      return {
        ...state,
        isLoading: true,
      };

    case LOAD_USER_SUGGESTIONS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        searchResults: action.payload.results,
      };

    case LOAD_USER_SUGGESTIONS_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };

    case RESET_AFTER_SAVE_GROUP:
      return {
        ...state,
        isLoading: false,
        groupId: null,
        editGroupSuccess: false,
      };

    case EDIT_GROUP:
      return {
        ...state,
        isLoading: true,
      };

    case EDIT_GROUP_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        editGroupSuccess: true,
      };

    case EDIT_GROUP_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };

    default:
      return state;
  }
}
