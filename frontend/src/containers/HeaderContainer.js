import { useSelector, useDispatch } from "react-redux";
import React from "react";
import { logout } from "../actions/auth";
import { setCollapsed } from "../actions/navigation";
import { Header } from "../components/layout/Header";

function HeaderContainer(props) {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);

  function handleLogoutClick(stringArg) {
    dispatch(logout());
  }

  function toggleCollapsed() {
    dispatch(setCollapsed(!props.collapsed));
  }

  return (
    <Header
      auth={auth}
      handleLogoutClick={handleLogoutClick}
      collapsed={props.collapsed}
      toggleCollapsed={toggleCollapsed}
    />
  );
}

export default HeaderContainer;
