import { useSelector, useDispatch } from "react-redux";
import React from "react";
import { useHistory } from "react-router-dom";
import { Groups } from "../components/modules/Groups";
import { Spinner } from "../components/modules/elements/Spinner";
import { setManageStatus, clearSpecificGroup } from "../actions/groups";

function GroupsContainer() {
  const dispatch = useDispatch();
  dispatch(clearSpecificGroup());
  dispatch(setManageStatus("create"));
  const isLoading = useSelector((state) => state.groups.isLoading);
  const collapsed = useSelector((state) => state.navigation.collapsed);
  const userGroups = useSelector((state) => state.groups.userGroups);
  const userName = useSelector((state) => state.profile.username);
  const history = useHistory();

  function routeToGroupManager() {
    history.push("/groupmanager");
  }

  return (
    <>
      {isLoading ? (
        <Spinner />
      ) : (
        <Groups
          collapsed={collapsed}
          userGroups={userGroups}
          routeToGroupManager={routeToGroupManager}
          userName={userName}
        />
      )}
    </>
  );
}

export default GroupsContainer;
