import { useSelector, useDispatch } from "react-redux";
import React, { useState } from "react";
import { register } from "../actions/auth";
import { Register } from "../components/accounts/Register";

function RegisterContainer() {
  const dispatch = useDispatch();

  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);

  function handleRegister() {
    dispatch(register({ username, email, password }));
  }

  return (
    <Register
      setUsername={setUsername}
      username={username}
      setPassword={setPassword}
      setEmail={setEmail}
      isAuthenticated={isAuthenticated}
      handleRegister={handleRegister}
    />
  );
}

export default RegisterContainer;
