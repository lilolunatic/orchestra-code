import React, { useState, useEffect } from "react";
import ScoreModal from "../components/modules/elements/ScoreModal";
import { useDispatch, useSelector } from "react-redux";
import { createScore, resetCreated } from "../actions/score";
import { useHistory } from "react-router-dom";

function ScoreModalContainer(props) {
  const dispatch = useDispatch();
  const [confirmLoading, setConfirmLoading] = useState(false);
  const groupInstruments = useSelector(
    (state) => state.groups.groupInstruments
  );
  const groupId = useSelector((state) => state.groups.groupId);
  const created = useSelector((state) => state.score.created);
  const scoreId = useSelector((state) => state.score.scoreId);
  const history = useHistory();

  useEffect(() => {
    if (created) {
      routeToCreatedScore();
    }
  }, [created]);

  function routeToCreatedScore() {
    history.push(`/scores/${scoreId}`);
    dispatch(resetCreated());
  }

  function handleOk(scoreName, sheets) {
    dispatch(createScore(scoreName, sheets, groupId));
    props.setVisible(false);
  }

  function handleCancel() {
    props.setVisible(false);
  }

  return (
    <ScoreModal
      visible={props.visible}
      title={props.title}
      handleOk={handleOk}
      confirmLoading={confirmLoading}
      handleCancel={handleCancel}
      groupInstruments={groupInstruments}
    />
  );
}

export default ScoreModalContainer;
