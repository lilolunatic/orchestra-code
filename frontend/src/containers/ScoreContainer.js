import { useSelector, useDispatch } from "react-redux";
import React from "react";
import { Spinner } from "../components/modules/elements/Spinner";

import { Score } from "../components/modules/Score";

function ScoreContainer() {
  const loading = useSelector((state) => state.score.isLoading);
  const collapsed = useSelector((state) => state.navigation.collapsed);
  const sheets = useSelector((state) => state.score.sheets);
  const scoreName = useSelector((state) => state.score.scoreName);
  const scoreId = useSelector((state) => state.score.scoreId);
  const userRole = useSelector((state) => state.groups.userRole);

  return (
    <>
      {loading ? (
        <Spinner />
      ) : (
        <Score
          collapsed={collapsed}
          scoreName={scoreName}
          sheets={sheets}
          scoreId={scoreId}
          isConductor={userRole === "co"}
        />
      )}
    </>
  );
}

export default ScoreContainer;
