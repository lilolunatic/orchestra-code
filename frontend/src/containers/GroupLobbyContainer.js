import { useSelector, useDispatch } from "react-redux";
import React, { useEffect } from "react";
import { GroupLobby } from "../components/modules/GroupLobby";
import { useParams } from "react-router";
import { Spinner } from "../components/modules/elements/Spinner";
import {
  setUserGroupInstruments,
  setManageStatus,
  setUserRoleInGroup,
  loadUserGroups,
} from "../actions/groups";
import { userInstrumentSelector } from "../selectors/userInstrumentSelector";
import { useHistory } from "react-router-dom";

function GroupLobbyContainer() {
  const dispatch = useDispatch();

  let { id } = useParams();

  const loading = useSelector((state) => state.groups.isLoading);
  const collapsed = useSelector((state) => state.navigation.collapsed);
  const groupName = useSelector((state) => state.groups.groupName);
  const members = useSelector((state) => state.groups.members);
  const instruments = useSelector((state) => state.groups.instruments);
  const userInstruments = useSelector(userInstrumentSelector);
  const userRole = useSelector((state) => state.groups.userRole);
  const userGroupInstruments = useSelector(
    (state) => state.groups.userGroupInstruments
  );
  const groupMember = useSelector((state) => state.groups.userName);
  const history = useHistory();

  useEffect(() => {
    handleGroupRoleSet();
  });

  function handleInitialGroupLobby() {
    dispatch(setManageStatus("edit"));
    handleGroupRoleSet();
  }

  function handleGroupRoleSet() {
    if (
      typeof members === "undefined" ||
      members === null ||
      members.length < 1
    ) {
      return;
    }
    const member = members.find((member) => member.name === groupMember);
    if (member.role === userRole) {
      return;
    }
    dispatch(setUserRoleInGroup(member.role));
  }

  function routeToGroupManager() {
    history.push("/groupmanager");
  }
  const scoreId = useSelector((state) => state.score.scoreId);
  const sheets = useSelector((state) => state.score.sheets);
  const groupScores = useSelector((state) => state.groups.groupScores);

  function handleUserGroupInstrumentsChange(instruments) {
    dispatch(setUserGroupInstruments(id, instruments));
  }

  return (
    <>
      {loading ? (
        <Spinner />
      ) : (
        <GroupLobby
          id={id}
          collapsed={collapsed}
          groupName={groupName}
          members={members}
          instruments={instruments}
          userInstruments={userInstruments}
          userGroupInstruments={userGroupInstruments}
          handleUserGroupInstrumentsChange={handleUserGroupInstrumentsChange}
          routeToGroupManager={routeToGroupManager}
          groupScores={groupScores}
          userRole={userRole}
          handleInitialGroupLobby={handleInitialGroupLobby}
        />
      )}
    </>
  );
}

export default GroupLobbyContainer;
