import { useSelector, useDispatch } from "react-redux";
import React, { useState } from "react";
import { login } from "../actions/auth";
import { Login } from "../components/accounts/Login";

function LoginContainer() {
  const dispatch = useDispatch();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);

  function handleLogin() {
    dispatch(login(username, password));
  }

  return (
    <Login
      setUsername={setUsername}
      username={username}
      setPassword={setPassword}
      passoword={password}
      isAuthenticated={isAuthenticated}
      handleLogin={handleLogin}
    />
  );
}

export default LoginContainer;
