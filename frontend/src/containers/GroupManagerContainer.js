import { useSelector, useDispatch } from "react-redux";
import React, { useEffect } from "react";
import {
  loadUserSuggestions,
  setGroupData,
  resetGroupIdAfterSaveGroup,
  setEditGroupData,
} from "../actions/groupManager";
import { loadSpecificGroup, loadUserGroups } from "../actions/groups";

import { useHistory } from "react-router-dom";
import { GroupManager } from "../components/modules/GroupManager";
import { Spinner } from "../components/modules/elements/Spinner";

function GroupManagerContainer() {
  const dispatch = useDispatch();
  const collapsed = useSelector((state) => state.navigation.collapsed);
  const searchResults = useSelector(
    (state) => state.groupManager.searchResults
  );
  const userName = useSelector((state) => state.profile.username);
  const manageStatus = useSelector((state) => state.groups.manageStatus);
  const groupName = useSelector((state) => state.groups.groupName);
  const history = useHistory();
  const groupIdToEdit = useSelector((state) => state.groups.groupId);
  const groupIdAfterCreation = useSelector(
    (state) => state.groupManager.groupId
  );
  const initialMembers = useSelector((state) => state.groups.members);
  const editGroupSuccess = useSelector(
    (state) => state.groupManager.editGroupSuccess
  );
  const managerRole = useSelector((state) => state.groups.userRole);
  const isLoading = useSelector((state) => state.groups.isLoading);

  useEffect(() => {
    let createdGroupId = groupIdAfterCreation;
    if (
      typeof groupIdAfterCreation !== "undefined" &&
      groupIdAfterCreation !== null
    ) {
      dispatch(loadUserGroups());
      dispatch(loadSpecificGroup(createdGroupId, userName));
      routeToGroupLobby(groupIdAfterCreation);
      dispatch(resetGroupIdAfterSaveGroup());
    }
    if (editGroupSuccess) {
      dispatch(loadSpecificGroup(groupIdToEdit, userName));
      routeToGroupLobby(groupIdToEdit);
      dispatch(resetGroupIdAfterSaveGroup());
    }
  });

  function routeToGroupLobby(groupId) {
    history.push(`/groups/${groupId}`);
  }

  function handleUserSearch(searchSample) {
    dispatch(loadUserSuggestions(searchSample));
  }

  function handleSaveGroup(newGroupName, newMembers) {
    dispatch(setGroupData(newGroupName, newMembers));
  }

  function handleEditGroup(changedMembers) {
    dispatch(setEditGroupData(groupIdToEdit, changedMembers));
  }

  return (
    <>
      {isLoading ? (
        <Spinner />
      ) : (
        <GroupManager
          handleUserSearch={handleUserSearch}
          collapsed={collapsed}
          searchResults={searchResults}
          handleSaveGroup={handleSaveGroup}
          manageStatus={manageStatus}
          groupName={groupName}
          handleEditGroup={handleEditGroup}
          initialMembers={initialMembers}
          managerRole={managerRole}
        />
      )}
    </>
  );
}

export default GroupManagerContainer;
