import { useSelector, useDispatch } from "react-redux";
import React from "react";

import { Profile } from "../components/modules/Profile";
import { Spinner } from "../components/modules/elements/Spinner";
import { setUserInstrument, setProfileImage } from "../actions/profile";

function ProfileContainer() {
  const dispatch = useDispatch();
  const collapsed = useSelector((state) => state.navigation.collapsed);
  const username = useSelector((state) => state.profile.username);
  const instrumentDict = useSelector((state) => state.profile.instrumentDict);
  const profileImage = useSelector((state) => state.profile.profileImage);
  const isLoading = useSelector((state) => state.profile.isLoading);

  function handleInstrumentChange(instruments) {
    dispatch(setUserInstrument(instruments));
  }

  function setImage(img) {
    dispatch(setProfileImage(img));
  }

  return (
    <>
      {isLoading ? (
        <Spinner />
      ) : (
        <Profile
          collapsed={collapsed}
          username={username}
          instrumentDict={instrumentDict}
          handleInstrumentChange={handleInstrumentChange}
          profileImage={profileImage}
          setImage={setImage}
        />
      )}
    </>
  );
}

export default ProfileContainer;
