import axios from "axios";
import { tokenConfig } from "./auth";

import {
  LOAD_PROFILE_DATA,
  LOAD_PROFILE_DATA_SUCCESS,
  LOAD_PROFILE_DATA_FAILURE,
  SET_USER_INSTRUMENT,
  SET_USER_INSTRUMENT_SUCCESS,
  SET_USER_INSTRUMENT_FAILURE,
  SET_PROFILE_IMAGE,
  SET_PROFILE_IMAGE_SUCCESS,
  SET_PROFILE_IMAGE_FAILURE,
} from "./types";

export const loadProfileData = () => (dispatch, getState) => {
  // Profile Data
  dispatch({ type: LOAD_PROFILE_DATA });

  axios
    .get("/api/profile", tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: LOAD_PROFILE_DATA_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: LOAD_PROFILE_DATA_FAILURE,
      });
    });
};

export const setUserInstrument = (instruments) => (dispatch, getState) => {
  //Change User instruments + save in Backend
  dispatch({ type: SET_USER_INSTRUMENT });

  //Request Body
  const body = JSON.stringify({ name: instruments });

  axios
    .post("/api/profile", body, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: SET_USER_INSTRUMENT_SUCCESS,
        payload: { instruments },
      });
    })
    .catch((err) => {
      dispatch({
        type: SET_USER_INSTRUMENT_FAILURE,
      });
    });
};

export const setProfileImage = (img) => (dispatch, getState) => {
  //Change Profile Image and save in Backend
  dispatch({ type: SET_PROFILE_IMAGE });

  //Request Body
  const body = JSON.stringify({ profileImage: img });

  axios
    .post("/api/images", body, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: SET_PROFILE_IMAGE_SUCCESS,
        payload: { profileImage: img },
      });
    })
    .catch((err) => {
      dispatch({
        type: SET_PROFILE_IMAGE_FAILURE,
      });
    });
};
