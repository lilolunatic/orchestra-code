import axios from "axios";
import { tokenConfig } from "./auth";

import {
  LOAD_USER_GROUPS,
  LOAD_USER_GROUPS_SUCCESS,
  LOAD_USER_GROUPS_FAILURE,
  LOAD_SPECIFIC_GROUP,
  LOAD_SPECIFIC_GROUP_SUCCESS,
  LOAD_SPECIFIC_GROUP_FAILURE,
  SET_USER_GROUP_INSTRUMENTS,
  SET_USER_GROUP_INSTRUMENTS_SUCCESS,
  SET_USER_GROUP_INSTRUMENTS_FAILURE,
  INIT_GROUP_MANAGE_STATUS,
  SET_GROUP_ID,
  SET_USER_ROLE,
  CLEAR_SPECIFIC_GROUP,
} from "./types";

export const setGroupId = (id) => (dispatch) => {
  dispatch({ type: SET_GROUP_ID, payload: id });
};

export const loadUserGroups = () => (dispatch, getState) => {
  // Profile Data
  dispatch({ type: LOAD_USER_GROUPS });

  axios
    .get("/api/group", tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: LOAD_USER_GROUPS_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: LOAD_USER_GROUPS_FAILURE,
      });
    });
};

export const loadSpecificGroup = (id, userName) => (dispatch, getState) => {
  //Specific Group
  dispatch({ type: LOAD_SPECIFIC_GROUP, payload: userName });

  axios
    .get(`api/group/${id}`, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: LOAD_SPECIFIC_GROUP_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: LOAD_SPECIFIC_GROUP_FAILURE,
      });
    });
};

export const setUserGroupInstruments = (id, instruments) => (
  dispatch,
  getState
) => {
  //Manage instruments of Group
  dispatch({ type: SET_USER_GROUP_INSTRUMENTS });

  const body = JSON.stringify({ id, name: instruments });

  axios
    .post("/api/group/instruments", body, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: SET_USER_GROUP_INSTRUMENTS_SUCCESS,
        payload: instruments,
      });
    })
    .catch((err) => {
      dispatch({
        type: SET_USER_GROUP_INSTRUMENTS_FAILURE,
      });
    });
};

export const setManageStatus = (manageStatus) => (dispatch) => {
  dispatch({
    type: INIT_GROUP_MANAGE_STATUS,
    payload: manageStatus,
  });
};

export const setUserRoleInGroup = (userRole) => (dispatch) => {
  dispatch({
    type: SET_USER_ROLE,
    payload: userRole,
  });
};

export const clearSpecificGroup = () => (dispatch) => {
  dispatch({
    type: CLEAR_SPECIFIC_GROUP,
  });
};
