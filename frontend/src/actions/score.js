import axios from "axios";
import { tokenConfig, mediaTokenConfig } from "./auth";
import {
  CREATE_SCORE,
  CREATE_SCORE_SUCCESS,
  CREATE_SCORE_FAILURE,
  RESET_CREATED,
  LOAD_SCORE,
  LOAD_SCORE_SUCCESS,
  LOAD_SCORE_FAILURE,
} from "./types";

export const resetCreated = () => (dispatch) => {
  dispatch({ type: RESET_CREATED });
};

export const loadScore = (id, scoreName) => (dispatch, getState) => {
  dispatch({ type: LOAD_SCORE, payload: { id: id, scoreName: scoreName } });

  axios
    .get(`api/score/${id}`, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: LOAD_SCORE_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: LOAD_SCORE_FAILURE,
      });
    });
};

export const createScore = (name, sheets, groupId) => (dispatch, getState) => {
  dispatch({ type: CREATE_SCORE });

  // //Request Body

  let body = JSON.stringify({
    groupId,
    name,
    sheets,
  });

  axios
    .post("/api/score", body, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: CREATE_SCORE_SUCCESS,
        payload: { id: res.data, sheets, scoreName: name },
      });
    })
    .catch((err) => {
      dispatch({
        type: CREATE_SCORE_FAILURE,
      });
    });
};
