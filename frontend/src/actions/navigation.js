import { SET_COLLAPSED } from "./types";

export const setCollapsed = (collapsed) => (dispatch) => {
  dispatch({ type: SET_COLLAPSED, payload: collapsed });
};
