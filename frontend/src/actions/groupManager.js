import axios from "axios";
import { tokenConfig } from "./auth";
import {
  LOAD_USER_SUGGESTIONS,
  LOAD_USER_SUGGESTIONS_SUCCESS,
  LOAD_USER_SUGGESTIONS_FAILURE,
  CREATE_GROUP,
  CREATE_GROUP_SUCCESS,
  CREATE_GROUP_FAILURE,
  RESET_AFTER_SAVE_GROUP,
  EDIT_GROUP,
  EDIT_GROUP_SUCCESS,
  EDIT_GROUP_FAILURE,
  SET_USER_INSTRUMENT_SUCCESS,
} from "./types";

export const loadUserSuggestions = (searchSample) => (dispatch, getState) => {
  dispatch({ type: LOAD_USER_SUGGESTIONS });

  axios
    .get(`api/user?search=${searchSample}`, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: LOAD_USER_SUGGESTIONS_SUCCESS,
        payload: res.data,
      });
    })
    .catch(() => {
      dispatch({
        type: LOAD_USER_SUGGESTIONS_FAILURE,
      });
    });
};

export const setGroupData = (groupName, members) => (dispatch, getState) => {
  dispatch({ type: CREATE_GROUP });

  //Request Body
  const body = JSON.stringify({ users: members, groupName: groupName });

  axios
    .post("/api/group", body, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: CREATE_GROUP_SUCCESS,
        payload: res.data.id,
      });
    })
    .catch(() => {
      dispatch({
        type: CREATE_GROUP_FAILURE,
      });
    });
};

export const setEditGroupData = (groupId, cheangedMembers) => (
  dispatch,
  getState
) => {
  dispatch({ type: EDIT_GROUP });

  const body = JSON.stringify({ users: cheangedMembers });

  const urlBase = "/api/group/users/";

  const url = urlBase.concat(groupId);

  axios
    .post(url, body, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: EDIT_GROUP_SUCCESS,
      });
    })
    .catch(() => {
      dispatch({
        type: EDIT_GROUP_FAILURE,
      });
    });
};

export const resetGroupIdAfterSaveGroup = () => (dispatch) => {
  dispatch({ type: RESET_AFTER_SAVE_GROUP });
};
