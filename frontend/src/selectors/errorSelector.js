import { createSelector } from "reselect";

export const isErrorSelector = createSelector(
  [
    (state) => state.auth.isError,
    (state) => state.profile.isError,
    (state) => state.groups.isError,
  ],
  (authError, profileError, groupError) => {
    return authError || profileError || groupError;
  }
);
