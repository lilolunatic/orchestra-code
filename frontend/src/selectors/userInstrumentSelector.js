import { createSelector } from "reselect";

export const userInstrumentSelector = createSelector(
  [(state) => state.profile.instrumentDict],
  (instDict) => {
    const userInst = [];
    Object.keys(instDict).forEach((i) => {
      if (instDict[i]) {
        userInst.push(i);
      }
    });

    return userInst;
  }
);
