import { initialState } from "../../reducers/profile";
import { userInstrumentSelector } from "../userInstrumentSelector";

describe("Selector tests", () => {
  it("return error state", () => {
    const state = {
      ...initialState,
      profile: {
        instrumentDict: { violin: true, guitar: false },
      },
    };

    const result = ["violin"];

    expect(result).toEqual(userInstrumentSelector(state));
  });
});
