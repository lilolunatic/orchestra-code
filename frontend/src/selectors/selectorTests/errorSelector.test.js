import { initialState } from "../../reducers/auth";
import { useSelector } from "react-redux";
import { isErrorSelector } from "../errorSelector";

describe("Selector tests", () => {
  it("return error state - auth error", () => {
    const state = {
      ...initialState,
      auth: {
        isError: true,
      },
      profile: {
        isError: false,
      },
      groups: {
        isError: false,
      },
    };

    const result = true;

    expect(result).toEqual(isErrorSelector(state));
  });

  it("return error state - profile error", () => {
    const state = {
      ...initialState,
      auth: {
        isError: false,
      },
      profile: {
        isError: true,
      },
      groups: {
        isError: false,
      },
    };

    const result = true;

    expect(result).toEqual(isErrorSelector(state));
  });

  it("return error state - groups error", () => {
    const state = {
      ...initialState,
      auth: {
        isError: false,
      },
      profile: {
        isError: false,
      },
      groups: {
        isError: true,
      },
    };

    const result = true;

    expect(result).toEqual(isErrorSelector(state));
  });

  it("return error state - no error", () => {
    const state = {
      ...initialState,
      auth: {
        isError: false,
      },
      profile: {
        isError: false,
      },
      groups: {
        isError: false,
      },
    };

    const result = false;

    expect(result).toEqual(isErrorSelector(state));
  });
});
