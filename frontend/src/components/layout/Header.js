import React from "react";
import { Link } from "react-router-dom";
import { Button } from "antd";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";

export function Header(props) {
  const { isAuthenticated, user } = props.auth;

  function clickLogout() {
    props.handleLogoutClick();
  }

  return (
    <nav className="navbar navbar-expand-sm navbar-light bg-light">
      <div className="container" style={{ width: "100%" }}>
        {isAuthenticated && (
          <Button
            type="primary"
            onClick={props.toggleCollapsed}
            style={{ marginRight: "35px" }}
          >
            {props.collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
          </Button>
        )}

        <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
          <a className="navbar-brand" href="#">
            Orchestra
          </a>
        </div>
        {isAuthenticated ? (
          <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
            <span className="navbar-text mr-3">
              <strong>{user ? `Welcome ${user.username}` : ""}</strong>
            </span>
            <li className="nav-item">
              <Button type="primary" onClick={() => clickLogout()}>
                Logout
              </Button>
            </li>
          </ul>
        ) : (
          <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
            <li className="nav-item" style={{ marginRight: "10px" }}>
              <Link to="/register" className="nav-Link">
                Register
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/login" className="nav-Link">
                Login
              </Link>
            </li>
          </ul>
        )}
      </div>
    </nav>
  );
}
