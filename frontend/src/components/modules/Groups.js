import React, { useState, useEffect } from "react";
import GroupCard from "./elements/GroupCard";
import { Button } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import Module from "./elements/ModuleLayout";

export function Groups(props) {
  return (
    <>
      <Module
        title={"My Groups"}
        content={
          <div
            position="absolute"
            style={{
              width: "100%",
              height: "90%",
              display: "flex",
              flexDirection: "row",
              flexWrap: "wrap",
              alignItems: "flex-start",
              overflow: "auto",
            }}
          >
            {props.userGroups.map((g) => {
              return (
                <GroupCard
                  title={g.name}
                  key={g.id}
                  id={g.id}
                  linkUrl={`/groups/${g.id}`}
                  userName={props.userName}
                />
              );
            })}
          </div>
        }
      />
      <Button
        id="new-group"
        type="primary"
        shape="circle"
        style={{
          position: "absolute",
          right: "1.5%",
          bottom: "10%",
          width: "60px",
          height: "60px",
          boxShadow: "0 0 12px #626262",
        }}
        onClick={props.routeToGroupManager}
      >
        <FontAwesomeIcon icon={faPlus} size="lg" />
      </Button>
    </>
  );
}
