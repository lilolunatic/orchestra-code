import React from "react";
import { Button } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen } from "@fortawesome/free-solid-svg-icons";
import Module from "./elements/ModuleLayout";
import ScoreCard from "./elements/ScoreCard";

export function Score(props) {
  return (
    <>
      <Module
        title={props.scoreName}
        content={
          <div
            position="absolute"
            style={{
              width: "100%",
              height: "90%",
              display: "flex",
              flexDirection: "row",
              flexWrap: "wrap",
              alignItems: "flex-start",
              overflow: "auto",
            }}
          >
            {Object.keys(props.sheets).map((s) => {
              console.log(props.sheets);
              return (
                <ScoreCard
                  title={props.sheets[s].name}
                  key={props.sheets[s].id}
                  isConductor={props.sheets[s].isConductor}
                />
              );
            })}
          </div>
        }
      />
      <Button
        type="primary"
        shape="circle"
        style={{
          position: "absolute",
          right: "1.5%",
          bottom: "10%",
          width: "60px",
          height: "60px",
          boxShadow: "0 0 12px #626262",
        }}
      >
        <FontAwesomeIcon icon={faPen} size="lg" />
      </Button>
    </>
  );
}
