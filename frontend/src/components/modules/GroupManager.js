import React, { useState, useEffect } from "react";
import { Form, Input, Button, Row, Col, Descriptions } from "antd";
import {
  UserAddOutlined,
  TeamOutlined,
  UsergroupAddOutlined,
} from "@ant-design/icons";
import Module from "./elements/ModuleLayout";
import UserManager from "./elements/UserManager";

export function GroupManager(props) {
  const [groupMembers, setGroupMembers] = useState([]);
  const [validations, setValidations] = useState([]);
  const [componentDidMount, setComponentDidMount] = useState(false);
  const [groupName, setGroupName] = useState("");

  const managerRole = props.managerRole;
  const manageStatus = props.manageStatus;

  useEffect(() => {
    if (componentDidMount) {
      return;
    }
    setComponentDidMount(true);
    setInitialMembers();
  });

  function setInitialMembers() {
    let dict = [];
    let vals = [];
    props.initialMembers.forEach((member) => {
      const key = createGuid();
      dict.push({
        key: key,
        member: {
          id: member.id,
          name: member.name,
          role: member.role,
        },
      });
      vals[key] = {
        validation: "success",
        message: null,
      };
    });
    setGroupMembers(dict);
    setValidations(vals);
  }

  function createGuid() {
    function getRandom() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    return (
      getRandom() +
      getRandom() +
      "-" +
      getRandom() +
      "-4" +
      getRandom().substr(0, 3) +
      "-" +
      getRandom() +
      "-" +
      getRandom() +
      getRandom() +
      getRandom()
    ).toLowerCase();
  }

  function handleSearch(searchSample) {
    props.handleUserSearch(searchSample);
  }

  function saveGroup() {
    if (manageStatus === "create") {
      const members = groupMembers.map((member) => {
        return { id: member.member.id, role: member.member.role };
      });
      props.handleSaveGroup(groupName, members);
    }
    if (manageStatus === "edit") {
      props.handleEditGroup(getChangedMembers());
    }
  }

  function getChangedMembers() {
    let changedMembers = [];
    groupMembers.forEach((updatedMember) => {
      if (
        (props.initialMembers.some(
          (existingMember) =>
            existingMember.id === updatedMember.member.id &&
            existingMember.role !== updatedMember.member.role
        ) ||
          !props.initialMembers.some(
            (existingMember) => existingMember.id === updatedMember.member.id
          )) &&
        updatedMember.id !== ""
      ) {
        changedMembers.push({
          id: updatedMember.member.id,
          action: updatedMember.member.role,
        });
      }
    });
    props.initialMembers.forEach((existingMember) => {
      if (
        !groupMembers.some(
          (updatedMember) => updatedMember.member.id === existingMember.id
        )
      ) {
        changedMembers.push({ id: existingMember.id, action: "del" });
      }
    });

    return changedMembers;
  }

  function setValidationSuccess(key) {
    setValidation("success", key, null);
  }

  function setValidationWarning(key) {
    setValidation(
      "warning",
      key,
      "please select a member or delete this field"
    );
  }

  function setValidationError(key) {
    setValidation("error", key, "User is already member of this group.");
  }

  function setValidation(validation, key, message) {
    let vals = $.extend(true, {}, validations);
    vals[key] = {
      validation: validation,
      message: message,
    };
    setValidations(vals);
    return;
  }

  function getValidation(key) {
    if (!Object.keys(validations).includes(key.toString())) {
      return "";
    }
    return validations[key].validation;
  }

  function getValidationMessage(key) {
    if (!Object.keys(validations).includes(key.toString())) {
      return "";
    }
    return validations[key].message;
  }

  function changeMemberName(user, index) {
    let members = Array.from(groupMembers);
    let uid = props.searchResults.filter((u) => u.username === user)[0].id;
    members[index].member.name = user;
    members[index].member.id = uid;

    setGroupMembers(members);
  }

  function addMember(user, key) {
    const index = getIndexOfKeyInArray(groupMembers, key);

    if (!groupMembers.some((groupMember) => groupMember.member.name === user)) {
      changeMemberName(user, index);
      setValidationSuccess(key);
      return;
    }
    if (
      groupMembers.some(
        (member) => member.key === key && member.member.name === user
      )
    ) {
      setValidationSuccess(key);
      return;
    }
    setValidationError(key);
  }

  function handleUserRole(permission, key) {
    if (
      groupMembers.some((groupMember) => groupMember.key === key.toString())
    ) {
      let members = Array.from(groupMembers);
      const index = getIndexOfKeyInArray(members, key);
      members[index].member.role = permission;
      setGroupMembers(members);
      return;
    }
  }

  function removeMember(key) {
    let members = Array.from(groupMembers);
    const membersIndex = getIndexOfKeyInArray(members, key);
    delete members[membersIndex];
    const filteredMembers = members.filter(
      (element) => element !== "undefined" && element !== null
    );
    setGroupMembers(filteredMembers);

    let vals = $.extend(true, {}, validations);
    delete vals[key];
    setValidations(vals);
  }

  function getIndexOfKeyInArray(arrayOfObjects, key) {
    return arrayOfObjects.findIndex((obj) => {
      return obj.key === key;
    });
  }

  function renderGroupName() {
    if (manageStatus === "edit") {
      return (
        <Descriptions>
          <Descriptions.Item style={{ fontWeight: "bold" }} label="Group Name">
            {props.groupName}
          </Descriptions.Item>
        </Descriptions>
      );
    }
    return (
      <Form name="group_name_form">
        <Form.Item name="goup_name_input" hasFeedback validateStatus="">
          <Input
            onChange={(e) => setGroupName(e.target.value)}
            placeholder="Group Name"
            size="large"
            style={{ width: 300 }}
            prefix={<TeamOutlined />}
          />
        </Form.Item>
      </Form>
    );
  }

  function addNewMember() {
    const key = createGuid();
    let dict = Array.from(groupMembers);
    dict.push({
      key: key,
      member: {
        id: "",
        name: "",
        role: "mu",
      },
    });
    setGroupMembers(dict);

    let vals = $.extend(true, {}, validations);
    vals[key] = {
      validation: "",
      message: "",
    };
    setValidations(vals);
  }

  return (
    <Module
      title={props.title}
      content={
        <div>
          <Row>
            <Col>{renderGroupName()}</Col>
          </Row>
          <Form name="dynamic_form_item">
            {groupMembers.map((groupMember) => {
              if (typeof groupMember === "undefined" || groupMember === null) {
                return;
              }
              return (
                <UserManager
                  key={groupMember.key}
                  memberId={groupMember.key}
                  memberName={groupMember.member.name}
                  memberRole={groupMember.member.role}
                  searchResults={props.searchResults}
                  managerRole={managerRole}
                  manageStatus={manageStatus}
                  getValidation={getValidation}
                  getValidationMessage={getValidationMessage}
                  addMember={addMember}
                  setValidationWarning={setValidationWarning}
                  handleUserRole={handleUserRole}
                  handleSearch={handleSearch}
                  removeMember={removeMember}
                />
              );
            })}

            <Form.Item>
              <div id="addUser">
                <Button
                  type="dashed"
                  onClick={() => {
                    addNewMember();
                  }}
                  style={{ width: 300 }}
                >
                  <UserAddOutlined /> Add user
                </Button>
              </div>
            </Form.Item>

            <Form.Item>
              <Button type="primary" onClick={() => saveGroup()}>
                <UsergroupAddOutlined />
                Save
              </Button>
            </Form.Item>
          </Form>
        </div>
      }
    />
  );
}
