import React, { useState, useEffect } from "react";
import { Button, Select, List, Avatar } from "antd";
import Module from "./elements/ModuleLayout";
import ScoreCard from "./elements/ScoreCard";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen } from "@fortawesome/free-solid-svg-icons";
import GroupInstrumentSelector from "./elements/GroupInstrumentSelector";
import MemberList from "./elements/MemberList";
import ScoreList from "./elements/ScoreList";

export function GroupLobby(props) {
  const [componentDidMount, setComponentDidMount] = useState(false);

  useEffect(() => {
    if (componentDidMount) {
      return;
    }
    setComponentDidMount(true);
    props.handleInitialGroupLobby();
  });

  function handleInstrumentsChange(instruments) {
    props.handleUserGroupInstrumentsChange(instruments);
  }

  const defaultInstruments = props.userInstruments.map((ui) => {
    return (
      <Select.Option key={ui} id={ui}>
        {ui}
      </Select.Option>
    );
  });

  const isConductor = () => {
    return props.userRole === "co";
  };

  const hasPermissionToManageUsers = () => {
    return isConductor() || props.userRole === "ma";
  };

  return (
    <>
      <Module
        title={props.groupName}
        content={
          <div
            style={{
              width: "100%",
              height: "100%",
              justifyContent: "space-between",
              display: "flex",
              flexDirection: "row",
            }}
          >
            <div style={{ width: "65%", height: "100%", display: "block" }}>
              <GroupInstrumentSelector
                defaultInstruments={defaultInstruments}
                handleInstrumentsChange={handleInstrumentsChange}
                userGroupInstruments={props.userGroupInstruments}
              />

              <ScoreList
                groupId={props.Id}
                groupScores={props.groupScores}
                isConductor={hasPermissionToManageUsers()}
              />
            </div>

            <MemberList members={props.members} />
          </div>
        }
      />
      {hasPermissionToManageUsers() && (
        <Button
          type="primary"
          shape="circle"
          style={{
            position: "absolute",
            right: "1.5%",
            bottom: "10%",
            width: "60px",
            height: "60px",
            boxShadow: "0 0 12px #626262",
          }}
          onClick={props.routeToGroupManager}
        >
          <div id="editGroup">
            <FontAwesomeIcon icon={faPen} size="lg" />
          </div>
        </Button>
      )}
    </>
  );
}
