import React, { useState } from "react";
import { Modal, Input, Form, Button, Select, Row, Upload, message } from "antd";
import { MinusCircleOutlined, UploadOutlined } from "@ant-design/icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

export default function ScoreModal(props) {
  //scoreDict = [{index: X, sheet:{name:"...", instrument: "...", file:"..."}}]
  const [scoreDict, setScoreDict] = useState([]);
  const [scoreName, setScoreName] = useState("");
  const [loading, setLoading] = useState(false);
  let opt = instOptions();

  function instOptions() {
    if (!props.groupInstruments) {
      return [];
    }
    return props.groupInstruments.map((i) => {
      return (
        <Select.Option key={i} value={i}>
          {i}
        </Select.Option>
      );
    });
  }

  function addSheet(sheet, index) {
    let dict = scoreDict;
    if (Object.keys(dict).includes(index.toString())) {
      dict[index].sheet.name = sheet;
      setScoreDict(dict);
      return;
    }

    dict.push({
      index: index,
      sheet: {
        name: sheet,
        instrument: "",
        file: null,
      },
    });
    setScoreDict(dict);
  }

  function selectInstrument(instrument, index) {
    let dict = scoreDict;

    if (Object.keys(scoreDict).includes(index.toString())) {
      dict[index].sheet.instrument = instrument;
      setScoreDict(dict);
      return;
    }

    dict.push({
      index: index,
      sheet: {
        name: null,
        instrument: instrument,
        file: null,
      },
    });
    setScoreDict(dict);
  }

  function removeScore(index) {
    let dict = scoreDict;
    delete dict[index].sheet;
    delete dict[index];
    setScoreDict(dict);
  }

  function handleOk() {
    let sheets = Object.keys(scoreDict).map((index) => {
      return scoreDict[index].sheet;
    });
    props.handleOk(scoreName, sheets);
  }

  function handleCancel() {
    props.handleCancel();
  }

  function uploadWithFormData(info, index) {
    if (info.file.status === "done") {
      let dict = scoreDict;
      let file = getBase64(info.file.originFileObj, (f) => {
        if (Object.keys(scoreDict).includes(index.toString())) {
          dict[index].sheet.file = f;
          setScoreDict(dict);
          return;
        }

        dict.push({
          index: index,
          sheet: {
            name: null,
            instrument: "",
            file: f,
          },
        });
      });

      setScoreDict(dict);
    }
  }

  function beforeUpload(file) {
    const isXml = file.type === "text/xml";
    if (!isXml) {
      message.error("You can only upload xml files!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error("File must smaller than 2MB!");
    }
    return isXml && isLt2M;
  }

  function getBase64(file, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    return reader.readAsDataURL(file);
  }

  return (
    <Modal
      title={props.title}
      visible={props.visible}
      onOk={handleOk}
      confirmLoading={props.confirmLoading}
      onCancel={handleCancel}
      height="80%"
      width="50%"
    >
      <div>
        <div style={{ display: "block" }}>
          <label style={{ marginRight: "10px" }}>Score Name</label>
          <div id="score-name">
            <Input
              onChange={(e) => setScoreName(e.target.value)}
              placeholder="Enter Score Name"
              size="large"
              style={{ width: 300, marginBottom: "30px" }}
            />
          </div>
          <Form name="dynamic_form_item">
            <div
              style={{
                diplay: "flex",
                widht: "100%",
                height: "80%",
              }}
            >
              <Form.List name="scores">
                {(fields, { add, remove }) => {
                  return (
                    <div>
                      {fields.map((field) => (
                        <Form.Item key={field.key}>
                          <Row>
                            <Form.Item noStyle key={field.key + "fi1"}>
                              <div style={{ height: "30px", width: "200px" }}>
                                <Input
                                  key={field.key + "Input"}
                                  onChange={(e) =>
                                    addSheet(e.target.value, field.key)
                                  }
                                  placeholder="add music sheet"
                                  size="middle"
                                  style={{
                                    width: 170,
                                    marginRight: "20px",
                                  }}
                                />
                              </div>
                            </Form.Item>
                            <Form.Item>
                              <div
                                style={{
                                  height: "30px",
                                  width: "200px",
                                  display: "inline-block",
                                }}
                              >
                                <Upload
                                  accept="text/xml"
                                  action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                                  beforeUpload={beforeUpload}
                                  onChange={(e) =>
                                    uploadWithFormData(e, field.key)
                                  }
                                  multiple={false}
                                >
                                  <Button>
                                    <UploadOutlined /> Upload
                                  </Button>
                                </Upload>
                              </div>
                            </Form.Item>
                            <Form.Item
                              {...field}
                              key={field.key + "fi2"}
                              validateTrigger={["onChange", "onSelect"]}
                              rules={[
                                {
                                  required: true,
                                  message: "add sheet to score",
                                },
                              ]}
                              noStyle
                            >
                              <Form.Item noStyle key={field.key + "fi3"}>
                                <div style={{ height: "30px", width: "200px" }}>
                                  <Select
                                    key={field.key + "Select"}
                                    defaultValue=""
                                    style={{
                                      width: 170,
                                      marginRight: "20px",
                                    }}
                                    //onChange={(e) => selectInstrument(e)}
                                    onSelect={(e) =>
                                      selectInstrument(e, field.key)
                                    }
                                  >
                                    {opt}
                                  </Select>
                                </div>
                              </Form.Item>
                            </Form.Item>
                            {fields.length > 1 ? (
                              <MinusCircleOutlined
                                key={field.key + "Remove"}
                                className="dynamic-delete-button"
                                style={{
                                  margin: "0 8px",
                                  color: "#FF0026",
                                  marginTop: "10px",
                                }}
                                onClick={() => {
                                  removeScore(field.key);
                                  remove(field.name);
                                }}
                              />
                            ) : null}
                          </Row>
                        </Form.Item>
                      ))}
                      <Form.Item>
                        <Button
                          type="dashed"
                          onClick={() => {
                            add();
                          }}
                          style={{
                            width: 300,
                            marginTop: "20px",
                            marginLeft: "30px",
                          }}
                        >
                          <FontAwesomeIcon
                            icon={faPlus}
                            size="lg"
                            style={{ marginRight: "10px" }}
                          />
                          Add Sheet
                        </Button>
                      </Form.Item>
                    </div>
                  );
                }}
              </Form.List>
            </div>
          </Form>
        </div>
      </div>
    </Modal>
  );
}
