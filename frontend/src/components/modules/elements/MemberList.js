import React from "react";
import { List, Avatar } from "antd";

export default function MemberList(props) {
  return (
    <div
      style={{
        display: "block",
        width: "30%",
        height: "100%",
      }}
    >
      <h3>Members</h3>
      <div
        style={{
          overflowY: "scroll",
          border: "solid",
          borderWidth: "1px",
          borderColor: "lightgray",
          padding: "5px",
          height: "415px",
        }}
      >
        <List
          itemLayout="horizontal"
          dataSource={props.members}
          renderItem={(member) => (
            <List.Item>
              <List.Item.Meta
                avatar={<Avatar src={member.profileImage} />}
                title={member.name}
              />
            </List.Item>
          )}
        />
      </div>
    </div>
  );
}
