import React, { useState } from "react";
import ScoreCard from "./ScoreCard";
import { Button } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import ScoreModalContainer from "../../../containers/ScoreModalContainer";

export default function ScoreList(props) {
  const [visible, setVisible] = useState(false);

  function showModal() {
    setVisible(true);
  }

  return (
    <div
      style={{
        display: "block",
        marginTop: "30px",
      }}
    >
      <div
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <h3>Music Scores</h3>
        {props.isConductor && (
          <Button
            type="primary"
            shape="circle"
            style={{
              width: "40px",
              height: "40px",
              marginBottom: "7px",
              marginRight: "10px",
            }}
            onClick={showModal}
          >
            <div id="new-score">
              <FontAwesomeIcon icon={faPlus} size="lg" />
            </div>
          </Button>
        )}
      </div>
      <ScoreModalContainer
        groupId={props.groupId}
        visible={visible}
        setVisible={setVisible}
        title={"Create New Score"}
      />

      <div
        style={{
          overflowY: "scroll",
          border: "solid",
          borderWidth: "1px",
          borderColor: "lightgray",
          paddingLeft: "15px",
          width: "100%",
          height: "300px",
          display: "flex",
          flexDirection: "row",
          flexWrap: "wrap",
          alignItems: "flex-start",
          justifyContent: "space-between",
        }}
      >
        {props.groupScores.map((s) => {
          return (
            <ScoreCard
              isConductor={props.isConductor}
              title={s.name}
              key={s.scoreId}
              id={s.scoreId}
              linkUrl={`/scores/${s.scoreId}`}
            />
          );
        })}
      </div>
    </div>
  );
}
