import React from "react";
import { Form, AutoComplete, Select, Row } from "antd";
import { UserDeleteOutlined } from "@ant-design/icons";

export default function UserManager(props) {
  const memberId = props.memberId;
  const memberName = props.memberName;
  const memberRole = props.memberRole;
  const searchResults = props.searchResults;
  const managerRole = props.managerRole;
  const manageStatus = props.manageStatus;

  const managerIsMusicAdmin = () => {
    return manageStatus === "edit" && managerRole === "ma";
  };

  const editDenied = () => {
    return managerIsMusicAdmin() && memberRole === "co";
  };

  return (
    <div>
      <Row>
        <Form.Item
          hasFeedback
          validateStatus={props.getValidation(memberId)}
          help={props.getValidationMessage(memberId)}
        >
          <AutoComplete
            style={{
              width: 300,
            }}
            placeholder={<div>user name</div>}
            onChange={(e) => props.handleSearch(e)}
            onSelect={(e) => props.addMember(e, memberId)}
            placeholder="user name"
            onKeyDown={() => props.setValidationWarning(memberId)}
            defaultValue={memberName}
            disabled={editDenied()}
          >
            {searchResults.map((user) => {
              return (
                <Select.Option key={user.id} value={user.username}>
                  {user.username}
                </Select.Option>
              );
            })}
          </AutoComplete>
        </Form.Item>
        <Form.Item key={(memberId, "_role")}>
          <Select
            defaultValue={memberRole}
            style={{ width: 150 }}
            onChange={(e) => props.handleUserRole(e, memberId)}
            disabled={editDenied()}
          >
            <Select.Option value="ma">music_admin</Select.Option>
            <Select.Option value="co" disabled={managerIsMusicAdmin()}>
              conductor
            </Select.Option>
            <Select.Option value="mu">musician</Select.Option>
          </Select>
        </Form.Item>
        {!editDenied() && (
          <div id="deleteUser">
            <UserDeleteOutlined
              className="dynamic-delete-button"
              style={{ margin: "0 8px" }}
              onClick={() => {
                props.removeMember(memberId);
              }}
            />
          </div>
        )}
      </Row>
    </div>
  );
}
