import React from "react";
import { Card, Button, Tooltip } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { loadSpecificGroup, setGroupId } from "../../../actions/groups";

export default function GroupCard(props) {
  const history = useHistory();
  const dispatch = useDispatch();

  const routeChange = () => {
    dispatch(setGroupId(props.id));
    dispatch(loadSpecificGroup(props.id, props.userName));
    history.push(props.linkUrl);
  };

  return (
    <Card
      title={props.title}
      key={props.id}
      bordered={true}
      style={{
        width: 400,
        borderColor: "#dcdcdc",
        marginRight: "10%",
        marginTop: "3%",
      }}
    >
      <div textalign="end">
        <Button
          type="primary"
          size="middle"
          style={{ marginRight: "250px" }}
          onClick={routeChange}
        >
          Enter
        </Button>
        <Tooltip title="Leave or Delete Group">
          <Button danger shape="circle" size="middle" padding="5px">
            <FontAwesomeIcon icon={faTimes} size="lg" />
          </Button>
        </Tooltip>
      </div>
    </Card>
  );
}
