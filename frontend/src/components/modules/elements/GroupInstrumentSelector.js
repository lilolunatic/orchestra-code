import React from "react";
import { Select } from "antd";

export default function GroupInstrumentSelector(props) {
  return (
    <div
      style={{
        display: "block",
      }}
    >
      <h3>My Group-Instuments</h3>
      <div
        style={{
          padding: "5px",
        }}
      >
        <Select
          mode="multiple"
          style={{ width: "100%" }}
          placeholder="Which instruments are you playing in this group?"
          defaultValue={props.userGroupInstruments}
          onChange={props.handleInstrumentsChange}
        >
          {props.defaultInstruments}
        </Select>
      </div>
    </div>
  );
}
