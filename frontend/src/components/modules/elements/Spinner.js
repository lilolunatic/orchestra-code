import React, { useState, useEffect } from "react";
import { Spin } from "antd";
import { useSelector } from "react-redux";

export function Spinner(props) {
  const collapsed = useSelector((state) => state.navigation.collapsed);
  return (
    <div
      position="absolute"
      style={{
        marginLeft: collapsed ? "150px" : "297px",
        width: "75%",
        height: "85%",
        backgroundColor: "white",
        border: "1px",
        borderColor: "lightgrey",
        padding: "30px",
        alignItems: "center",
        justifyContent: "space-around",
      }}
    >
      <Spin size="large" />
    </div>
  );
}
