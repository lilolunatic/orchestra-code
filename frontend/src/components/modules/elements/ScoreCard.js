import React from "react";
import { Card, Button, Tooltip } from "antd";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { loadScore } from "../../../actions/score";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLevelUpAlt } from "@fortawesome/free-solid-svg-icons";

export default function ScoreCard(props) {
  const history = useHistory();
  const dispatch = useDispatch();

  const routeChange = () => {
    dispatch(loadScore(props.id, props.title));
    history.push(`/scores/${props.id}`);
  };

  return (
    <Tooltip title={props.title}>
      <Card
        title={props.title}
        key={props.id}
        bordered={true}
        style={{
          width: 175,
          height: 120,
          borderColor: "#dcdcdc",
          marginRight: "5%%",
          marginTop: "3%",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Button
            type="primary"
            shape="circle"
            size="small"
            onClick={routeChange}
          >
            <FontAwesomeIcon icon={faLevelUpAlt} size="sm" />
          </Button>
        </div>
      </Card>
    </Tooltip>
  );
}
