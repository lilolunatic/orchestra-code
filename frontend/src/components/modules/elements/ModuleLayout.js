import React from "react";
import { Divider } from "antd";
import { useSelector } from "react-redux";

export default function Module(props) {
  const collapsed = useSelector((state) => state.navigation.collapsed);

  return (
    <div
      position="absolute"
      style={{
        marginLeft: collapsed ? "150px" : "297px",
        width: collapsed ? "84%" : "75%",
        height: "85%",
        backgroundColor: "white",
        border: "1px",
        borderColor: "lightgrey",
        padding: "30px",
      }}
    >
      <div
        position="absolute"
        display="flex"
        style={{ marginLeft: "10px", fontStyle: "bold" }}
        id={props.title}
      >
        <h2>{props.title}</h2>
        <hr border="4px" color={"#0099ff"} />
      </div>
      <div
        style={{
          width: "100%",
          height: "100%",
          display: "flex",
          overflowY: props.overflow,
          padding: "30px",
        }}
      >
        {props.content}
      </div>
    </div>
  );
}
