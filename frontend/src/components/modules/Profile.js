import React, { useState, useRef } from "react";
import { Button, Select, Upload, message } from "antd";
import Module from "./elements/ModuleLayout";
import axios from "axios";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";

export function Profile(props) {
  const defaultInstruments = [];
  const [loading, setLoading] = useState();
  const [url, setUrl] = useState();

  Object.keys(props.instrumentDict).map((d) => {
    defaultInstruments.push(
      <Select.Option key={d} id={d}>
        {d}
      </Select.Option>
    );
  });

  const userInstruments = Object.keys(props.instrumentDict).filter((i) => {
    if (props.instrumentDict[i]) {
      return i;
    }
  });

  function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  function beforeUpload(file) {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("You can only upload JPG/PNG file!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error("Image must smaller than 2MB!");
    }
    return isJpgOrPng && isLt2M;
  }

  function handlePictureChange(info) {
    if (info.file.status === "uploading") {
      setLoading(true);
      return;
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, (pic) => {
        setLoading(false);
        setUrl(pic);
        props.setImage(pic);
      });
    }
  }

  function handleChange(instruments) {
    props.handleInstrumentChange(instruments);
  }

  return (
    <Module
      title={"My Profile"}
      content={
        <div
          style={{
            width: "100%",
            height: "100%",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <div>
            <label style={{ fontWeight: "bold", fontSize: "20px" }}>
              Name:{" "}
            </label>{" "}
            <p>{props.username}</p>
            <label
              style={{
                fontWeight: "bold",
                marginTop: "30px",
                fontSize: "20px",
              }}
            >
              Instruments:
            </label>
            <div style={{ width: "250px" }} id="instrument-selection">
              <Select
                mode="multiple"
                style={{ width: "100%" }}
                placeholder="Please select"
                defaultValue={userInstruments}
                onChange={handleChange}
              >
                {defaultInstruments}
              </Select>
            </div>
          </div>
          <div style={{ marginRight: "200px" }}>
            <label
              style={{
                fontWeight: "bold",
                fontSize: "20px",
                marginBottom: "20px",
              }}
            >
              Upload profile picture:
            </label>{" "}
            <div style={{ alignItems: "center" }}>
              <Upload
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                beforeUpload={beforeUpload}
                onChange={(e) => handlePictureChange(e)}
              >
                {props.profileImage ? (
                  <img
                    src={props.profileImage}
                    alt="avatar"
                    style={{ width: "100%" }}
                  />
                ) : (
                  <div>
                    {loading ? <LoadingOutlined /> : <PlusOutlined />}
                    <div className="ant-upload-text">Upload</div>
                  </div>
                )}
              </Upload>
            </div>
          </div>
        </div>
      }
      overflow={"hidden"}
    />
  );
}
