import React, { useState } from "react";
import { Link } from "react-router-dom";
import "antd/dist/antd.css";
import { Menu, Avatar } from "antd";
import {
  TeamOutlined,
  DashboardOutlined,
  SmileOutlined,
  ReadOutlined,
} from "@ant-design/icons";
import { useSelector, useDispatch } from "react-redux";
import { loadUserGroups } from "../../actions/groups";

export function Sidebar() {
  const dispatch = useDispatch();
  const collapsed = useSelector((state) => state.navigation.collapsed);
  const profileImage = useSelector((state) => state.profile.profileImage);

  return (
    <div style={{ width: 256 }}>
      <Menu
        defaultSelectedKeys={["0"]}
        mode="inline"
        theme="dark"
        inlineCollapsed={collapsed}
        style={{
          position: "absolute",
          alignSelf: "bottom",
          height: "88%",
          width: collapsed ? 75 : 256,
        }}
      >
        <div
          style={{
            marginLeft: "25px",
            marginTop: "20px",
            marginBottom: "15px",
          }}
        >
          <Avatar size={100} src={profileImage} />
        </div>
        <Menu.Item key="0">
          <div>
            <Link to="/" />
            <DashboardOutlined />
            <span>Dashboard</span>
          </div>
        </Menu.Item>
        <Menu.Item id="profile" key="1">
          <Link id="profile-link" to="/profile" />
          <SmileOutlined />
          <span>My Profile</span>
        </Menu.Item>
        <Menu.Item id="groups" key="2" onClick={dispatch(loadUserGroups())}>
          <Link to="/groups" />
          <TeamOutlined />
          <span>My Groups</span>
        </Menu.Item>
        <Menu.Item key="3">
          <Link to="/files" />
          <ReadOutlined />
          <span>My Files</span>
        </Menu.Item>
      </Menu>
    </div>
  );
}
