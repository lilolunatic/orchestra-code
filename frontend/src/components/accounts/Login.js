import React from "react";
import { Link, Redirect } from "react-router-dom";

export function Login(props) {
  function onSubmit(e) {
    e.preventDefault();
    props.handleLogin();
  }

  function onUsernameChange(e) {
    props.setUsername(e.target.value);
  }
  function onPasswordChange(e) {
    props.setPassword(e.target.value);
  }

  return (
    <div>
      {props.isAuthenticated ? (
        <Redirect to="/" />
      ) : (
        <div className="col-md-6 m-auto">
          <div className="card card-body mt-5">
            <h2 className="text-center">Login</h2>
            <form onSubmit={onSubmit}>
              <div className="form-group">
                <label>Username</label>
                <input
                  type="text"
                  className="form-control"
                  name="username"
                  onChange={onUsernameChange}
                  value={props.username}
                />
              </div>
              <div className="form-group">
                <label>Password</label>
                <input
                  type="password"
                  className="form-control"
                  name="password"
                  onChange={onPasswordChange}
                  value={props.password}
                />
              </div>
              <div className="form-group">
                <button type="submit" className="btn btn-primary">
                  Login
                </button>
              </div>
              <p>
                Don't have an account? <Link to="/register">Sign up</Link>
              </p>
            </form>
          </div>
        </div>
      )}
    </div>
  );
}
