import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Form, Input, Button } from "antd";

export function Register(props) {
  const [submitOK, setSubmitOK] = useState(false);
  const [pwOkay, setPwOkay] = useState(false);

  function onSubmit() {
    props.handleRegister();
  }

  function onNameChange(e) {
    props.setUsername(e.target.value);
  }

  function onEmailChange(e) {
    props.setEmail(e.target.value);
  }

  useEffect(() => {
    if (props.username !== "" && props.email !== "" && pwOkay) {
      setSubmitOK(true);
    }
  }, [pwOkay, props.username, props.email]);

  return (
    <Form name="register" onFinish={onSubmit}>
      <div className="col-md-6 m-auto">
        <div className="card card-body mt-5">
          <h2 className="text-center">Register</h2>
          <div className="form-group">
            <label>Username</label>
            <Input
              name="username"
              onChange={(e) => onNameChange(e)}
              rules={[
                {
                  required: true,
                  message: "Please input your email!",
                },
              ]}
            />
          </div>
          <div className="form-group">
            <label>Email</label>
            <Input
              name="email"
              onChange={(e) => onEmailChange(e)}
              rules={[
                {
                  required: true,
                  message: "Please input your email!",
                },
              ]}
            />
          </div>
          <label>Password </label>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>

          <label>Confirm Password</label>
          <Form.Item
            name="confirm"
            dependencies={["password"]}
            hasFeedback
            rules={[
              {
                required: true,
                message: "Please confirm your password!",
              },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (!value || getFieldValue("password") === value) {
                    props.setPassword(value);
                    setPwOkay(true);

                    return Promise.resolve();
                  }

                  return Promise.reject(
                    "The two passwords that you entered do not match!"
                  );
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" disabled={!submitOK}>
              Register
            </Button>
          </Form.Item>
          <p>
            Already have an account? <Link to="/login">Login</Link>
          </p>
        </div>
      </div>
    </Form>
  );
}
