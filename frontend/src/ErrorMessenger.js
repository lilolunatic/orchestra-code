import React, { useEffect, Fragment } from "react";
import { useSelector } from "react-redux";
import { isErrorSelector } from "./selectors/errorSelector";
import { useAlert } from "react-alert";
//import { Alerts } from "../components/layout/Alerts";

function ErrorMessenger() {
  const isError = useSelector(isErrorSelector);

  const alert = useAlert();

  useEffect(() => {
    if (isError) {
      alert.show("Oopsie...");
    }
  }, [isError]);

  return <Fragment />;
}

export default ErrorMessenger;
