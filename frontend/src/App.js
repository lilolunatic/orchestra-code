import React, { Fragment } from "react";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import { Provider, useSelector, useDispatch } from "react-redux";
import { Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";

import RegisterContainer from "./containers/RegisterContainer";
import ErrorMessenger from "./ErrorMessenger";
import PrivateRoute from "./components/common/PrivateRoute";
import store from "./store";

import LoginContainer from "./containers/LoginContainer";
import GroupLobbyContainer from "./containers/GroupLobbyContainer";

import { Sidebar } from "./components/navigation/Sidebar";
import { Dashboard } from "./components/modules/Dashboard";
import GroupManagerContainer from "./containers/GroupManagerContainer";
import ProfileContainer from "./containers/ProfileContainer";
import GroupsContainer from "./containers/GroupsContainer";
import ScoreContainer from "./containers/ScoreContainer";
import { Files } from "./components/modules/Files";
import { loadProfileData } from "./actions/profile";
import { loadUserGroups } from "./actions/groups";
import HeaderContainer from "./containers/HeaderContainer";

//Alert Options
const errorOptions = {
  timeout: 5000,
  position: "top right",
  offset: "30px",
  type: "error",
};

export default function App() {
  const { user, isAuthenticated } = useSelector((state) => state.auth);
  const collapsed = useSelector((state) => state.navigation.collapsed);
  const dispatch = useDispatch();

  if (isAuthenticated) {
    dispatch(loadProfileData());
  }

  return (
    <div
      style={{
        width: "100vw",
        height: "100vh",
        backgroundColor: "#F5F5F5",
      }}
    >
      <Provider store={store}>
        <AlertProvider template={AlertTemplate} {...errorOptions}>
          <Router>
            <Fragment>
              <HeaderContainer collapsed={collapsed} />
              <ErrorMessenger />
              {user ? (
                <>
                  <Sidebar />
                  <Switch>
                    <PrivateRoute
                      exact
                      path="/dashboard"
                      component={Dashboard}
                      style={{ width: "100%", height: "100%" }}
                    />
                    <PrivateRoute
                      exact
                      path="/profile"
                      component={ProfileContainer}
                      style={{ width: "100%", height: "100%" }}
                    />
                    <PrivateRoute
                      exact
                      path="/groups"
                      component={GroupsContainer}
                      style={{ width: "100%", height: "100%" }}
                    />
                    <PrivateRoute
                      exact
                      path="/files"
                      component={Files}
                      style={{ width: "100%", height: "100%" }}
                    />
                    <PrivateRoute
                      exact
                      path="/groups/:id"
                      component={GroupLobbyContainer}
                      style={{ width: "100%", height: "100%" }}
                    />
                    <PrivateRoute
                      exact
                      path="/scores/:scoreId"
                      component={ScoreContainer}
                      style={{ width: "100%", height: "100%" }}
                    />

                    <PrivateRoute
                      exact
                      path="/groupmanager"
                      component={GroupManagerContainer}
                      style={{ width: "100%", height: "100%" }}
                    />
                  </Switch>
                </>
              ) : (
                <>
                  <Route exact path="/register" component={RegisterContainer} />
                  <Route exact path="/login" component={LoginContainer} />
                </>
              )}
            </Fragment>
          </Router>
        </AlertProvider>
      </Provider>
    </div>
  );
}
