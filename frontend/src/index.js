import App from "./App";
import "./index.css";
import "antd/dist/antd.css";
import store from "./store";
import { Provider } from "react-redux";
import React from "react";
import ReactDOM from "react-dom";

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app")
);
