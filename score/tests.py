import json

from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIClient

from modules.models import Group, Score, Sheet, Instrument, UserGroupRole


class ScoreTestCase(TestCase):
    def setUp(self):
        self.group = Group.objects.create(group_name="Gruppe")
        self.group2 = Group.objects.create(group_name="Gruppe2")

        self.score = Score.objects.create(name="Test Score", group_id=self.group.id)

        self.flute = Instrument.objects.create(instrument="flute")
        piano = Instrument.objects.create(instrument="piano")

        self.sheet = Sheet.objects.create(name="sheet_lol", instrument=self.flute, score=self.score)
        Sheet.objects.create(name="test", instrument=piano, score=self.score)

        user = User.objects.create(username="Test", password="1234")
        user.profile.instruments.add(self.flute)
        user.profile.instruments.add(piano)

        self.client = APIClient()
        self.client.force_authenticate(user=user)
        role = UserGroupRole.objects.create(profile=user.profile, group=self.group, role='co')
        role.instruments.add(self.flute)

        role.save()

    def test_get_request(self):
        response = self.client.get(f"/api/score/{self.score.id}")

        json_data = response.json.args[0].content.decode('utf8').replace("'", '"')
        json_data = json.loads(json_data)

        expected = {'sheets': [{'id': self.sheet.id, 'name': self.sheet.name, "instrument": self.flute.instrument}]}
        self.assertDictEqual(expected, json_data)

    def test_post_request(self):
        sheet_name = "Unique 123456789, xyz."
        score_count = Score.objects.all().count()

        response = self.client.post("/api/score",
                                    data={"groupId": self.group2.id,
                                          "name": "test post",
                                          "sheets": [{"name": sheet_name, "instrument": self.flute.instrument, "file": "data url jlfkjsökdfj"}]},
                                    format='json')

        expected = {'scoreId': score_count + 1,
                    'sheets': [{'id': Sheet.objects.get(name=sheet_name).id, 'name': sheet_name,
                                "instrument": self.flute.instrument}]}

        json_data = response.json.args[0].content.decode('utf8').replace("'", '"')
        json_data = json.loads(json_data)

        self.assertDictEqual(expected, json_data)

        # score with a save name should fail
        response = self.client.post("/api/score", data={"groupId": self.group2.id, "name": "test post", "sheets": []}, format='json')
        self.assertEqual(response.status_code, 400)
