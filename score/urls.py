from django.urls import path

from score.api import ScoreApi

urlpatterns = [
    path('api/score', ScoreApi.as_view()),
    path('api/score/<int:score_id>', ScoreApi.as_view()),

]