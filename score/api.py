from rest_framework import exceptions
from django.db import IntegrityError
from django.http import JsonResponse, HttpResponseServerError
from rest_framework import permissions
from rest_framework.views import APIView
from sheet.sheet_helpers import sheet_creator

from modules.models import Score, Instrument, Sheet, UserGroupRole


class ScoreApi(APIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    http_method_names = ['head', 'get', 'post']

    def get(self, request, score_id):
        user_instruments = Instrument.objects.filter(usergrouprole__profile__user=self.request.user)

        if UserGroupRole.objects.get(profile=self.request.user.profile, group__score=score_id).role == \
                UserGroupRole.CONDUCTOR:
            sheets = Sheet.objects.filter(score=score_id, instrument__in=user_instruments).values(
                'name', 'id', 'instrument__instrument')
        else:
            sheets = Sheet.objects.filter(score=score_id).values(
                'name', 'id', 'instrument__instrument')

        sheets = list(sheets)
        for sheet in sheets:
            sheet: dict
            sheet["instrument"] = sheet.pop("instrument__instrument")

        return JsonResponse({"sheets": sheets})

    def post(self, request):
        data = self.request.data

        try:
            score = Score.objects.create(
                group_id=data["groupId"], name=data["name"])
        except IntegrityError:
            raise exceptions.ValidationError(
                "Invalid name for Score. Name already exists")

        sheets = sheet_creator(data["sheets"], score.id)

        return JsonResponse({"scoreId": score.id, "sheets": sheets})
