from rest_framework import routers

from .api import InstrumentViewSet

router = routers.DefaultRouter()
router.register('api/instruments', InstrumentViewSet, 'instruments')

urlpatterns = router.urls
