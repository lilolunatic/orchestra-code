from rest_framework import permissions, viewsets
from .serializers import InstrumentSerializer
from modules.models import Instrument


class InstrumentViewSet(viewsets.ModelViewSet):

    permission_classes = [
        permissions.IsAuthenticated,
    ]

    serializer_class = InstrumentSerializer

    # request works despite error ¯\_(ツ)_/¯
    queryset = Instrument.objects.all()

    def perform_create(self, serializer):
        serializer.save()
