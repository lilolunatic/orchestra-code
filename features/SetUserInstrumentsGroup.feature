# Created by nhartmann at 6/19/2020
Feature: Manage user's instruments in group
  Scenario: Manage instruments in group
    Given I am logged in
    When I go to my Groups
    And I enter the instrumentTestGroup
    And I see the instrument selection
    Then I can add the xylophone