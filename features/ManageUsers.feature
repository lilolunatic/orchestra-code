# Created by nhartmann at 6/24/2020
Feature: Manage Users
  Scenario: Change role of one user and delete one user
    Given I am logged in
    When I go to my Groups
    When I enter the instrumentTestGroup
    And I click on the edit button
    Then I can change the role of a user
    And I can delete a user