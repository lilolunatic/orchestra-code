# Created by nhartmann at 6/19/2020
Feature: Create Score
  Scenario: Create Score
    Given I am logged in
    When I go to my Groups
    And I enter the ScoreTestGroup
    And I click on new Score
    And I enter the Scorename
    And I add sheets
    And I save the score
    Then I can see the score lobby
