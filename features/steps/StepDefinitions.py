from selenium import webdriver
from behave import *
import os

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

use_step_matcher("re")
driver = webdriver.Chrome()
wait = WebDriverWait(driver, 10)


@given("I am logged in")
def step_impl(context):
    driver.get("http://orchestra.xy:8000/#/login")
    driver.implicitly_wait(3)
    name_field = driver.find_element_by_name("username")
    name_field.send_keys("admin")
    password_field = driver.find_element_by_name("password")
    password_field.send_keys("1111")
    submit_button = driver.find_element_by_class_name("btn-primary")
    submit_button.click()
    driver.implicitly_wait(3)
    print("I should be logged in now")


@when("I go to my Profile")
def step_impl(context):
    profile_menu = driver.find_element_by_id("profile")
    profile_menu.click()
    driver.implicitly_wait(3)
    print("I should be on my profile now")


@when("I click on the instrument selection")
def step_impl(context):
    instrument_selection = driver.find_element_by_id("instrument-selection")
    instrument_selection.click()


@then("I can add the xylophone")
def step_impl(context):
    xylophone = driver.find_element_by_id("xylophone")
    xylophone.click()
    driver.implicitly_wait(5)
    print("I found the searched element")
    driver.quit()


@when("I go to my Groups")
def step_impl(context):
    group_menu = driver.find_element_by_id("groups")
    group_menu.click()
    driver.implicitly_wait(3)
    print("I should be on my groups now")


@when("I click on new Group")
def step_impl(context):
    new_group_button = driver.find_element_by_xpath('//*[@id="app"]/div/button')
    new_group_button.click()


@step("I enter the groupname")
def step_impl(context):
    group_name = driver.find_element_by_id("group_name_form_goup_name_input")
    group_name.send_keys("TestGroup")


@step("I add an user")
def step_impl(context):
    add_user_button = driver.find_element_by_id(("addUser"))
    add_user_button.click()
    username = driver.find_element_by_xpath('//*[@id="rc_select_0"]')
    username.send_keys(("testu"))
    (driver.implicitly_wait(2))
    testuser = driver.find_element_by_xpath("/html/body/div[3]/div/div/div/div[2]/div/div/div/div")
    testuser.click()
    role_selection = driver.find_element_by_xpath('//*[@id="rc_select_1"]')
    role_selection.click()
    role = driver.find_element_by_xpath('/html/body/div[4]/div/div/div/div[2]/div/div/div[2]/div')
    role.click()
    print("I added the user")


@step("I submit the new group")
def step_impl(context):
    save_button = driver.find_element_by_xpath('//*[@id="dynamic_form_item"]/div[3]/div/div/div/button')
    driver.implicitly_wait(3)
    save_button.click()
    print("I submitted the new group")


@then("I see the created group")
def step_impl(context):
    driver.implicitly_wait(5)
    driver.find_element_by_id("TestGroup")
    print("I am in the new grouplobby")
    driver.quit()


@step("I enter the instrumentTestGroup")
def step_impl(context):
    enter_group_button = driver.find_element_by_xpath(
        '//*[@id="app"]/div/div[2]/div[2]/div/div[1]/div[2]/div/button[1]')
    enter_group_button.click()
    driver.implicitly_wait(3)
    print("I am in group now")


@step("I see the instrument selection")
def step_impl(context):
    print("I see inst selection")
    instrument_selection_group = driver.find_element_by_xpath(
        '//*[@id="app"]/div/div[2]/div[2]/div/div[1]/div[1]/div/div/div')
    instrument_selection_group.click()


@step("I enter the ScoreTestGroup")
def step_impl(context):
    enter_group_button = driver.find_element_by_xpath(
        '//*[@id="app"]/div/div[2]/div[2]/div/div[4]/div[2]/div/button[1]')
    enter_group_button.click()
    driver.implicitly_wait(7)
    print("I am in group now")


@step("I click on new Score")
def step_impl(context):
    print("searching Button")
    new_score = driver.find_element_by_id("new-score")
    driver.implicitly_wait(7)
    new_score.click()
    driver.implicitly_wait(7)
    print("I clicked on new score")


@step("I enter the Scorename")
def step_impl(context):
    score_name = driver.find_element_by_xpath('//*[@id="score-name"]/input')
    score_name.send_keys("new Score")
    driver.implicitly_wait(1)
    print("I entered the scorename")


@step("I add sheets")
def step_impl(context):
    add_sheet_button = driver.find_element_by_xpath('//*[@id="dynamic_form_item"]/div/div/div/div/div/div/button')
    add_sheet_button.click()
    name_field = driver.find_element_by_xpath(
        '//*[@id="dynamic_form_item"]/div/div/div[1]/div/div/div/div/div[1]/input')
    name_field.send_keys("first instrument")
    upload_button = driver.find_element_by_xpath(
        '//*[@id="dynamic_form_item"]/div/div/div[1]/div/div/div/div/div[2]/div/div/div/div/span/div[1]/span/button')
    # upload_button.click()
    path = os.path.join("..", "..", "media", "Beethoven_AnDieFerneGeliebte.xml")
    upload_button.send_keys(path)
    print("upload ready")
    driver.find_element_by_xpath('//*[@id="rc_select_1"]').click()
    driver.implicitly_wait(2)
    instrument = driver.find_element_by_xpath('/html/body/div[4]/div/div/div/div[2]/div/div/div[1]/div')
    instrument.click()
    driver.implicitly_wait(2)
    print("I added a sheet with name, xml and instrument")


@step("I save the score")
def step_impl(context):
    save_button = driver.find_element_by_xpath('/html/body/div[3]/div/div[2]/div/div[2]/div[3]/div/button[2]')
    save_button.click()
    driver.implicitly_wait(7)
    print("I saved my new score")


@then("I can see the score lobby")
def step_impl(context):
    title = driver.find_element_by_id("new Score")
    print("I found the title, Im in the score-overview")
    driver.quit()


@step("I click on the edit button")
def step_impl(context):
    edit_button = driver.find_element_by_id("editGroup")
    driver.implicitly_wait(7)
    edit_button.click()
    driver.implicitly_wait(2)
    print("I clicked on edit")


@then("I can change the role of a user")
def step_impl(context):
    role_selection = driver.find_element_by_xpath('//*[@id="rc_select_4"]')
    role_selection.click()
    role = driver.find_element_by_xpath('/html/body/div[3]/div/div/div/div[2]/div/div/div[2]/div')
    role.click()
    driver.implicitly_wait(3)
    print("I changed the role now")


@step("I can delete a user")
def step_impl(context):
    delete_button = driver.find_element_by_id("deleteUser")
    delete_button.click()
    driver.implicitly_wait(3)
    print("I deleted one user")
    driver.quit()


@then("I can upload a picture")
def step_impl(context):
    upload = driver.find_element_by_xpath('//*[@id="app"]/div/div[2]/div[2]/div/div[2]/div/span/div/span')
    driver.implicitly_wait(3)
    path = os.path.join("..", "..", "media", "saxophone.png")
    upload.send_keys(path)
    driver.implicitly_wait(7)
    print("I uploaded a picture")
    driver.quit()
