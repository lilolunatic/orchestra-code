# Created by lilol at 18.06.2020
Feature: Create Group

  Scenario: Create Group
    Given I am logged in
    When I go to my Groups
    When I click on new Group
    And I enter the groupname
    And I add an user
    And I submit the new group
    Then I see the created group