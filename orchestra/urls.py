import os

from django.contrib import admin
from django.urls import path, include

from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('frontend.urls')),
    path('', include('accounts.urls')),
    path('', include('instruments.urls')),
    path('', include('group.urls')),
    path('', include('score.urls')),
    path('', include('sheet.urls')),
    path('', include('modules.urls')),
]

urlpatterns += static(f"{settings.MEDIA_URL}default/",
                      document_root=os.path.join(settings.MEDIA_ROOT, "default"))
