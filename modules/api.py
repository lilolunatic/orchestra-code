from django.http import JsonResponse, FileResponse
from rest_framework import permissions
from rest_framework.views import APIView

from modules.models import Profile


class ImageUploadView(APIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    http_method_names = ['head', 'post']

    def post(self, request, *args, **kwargs):
        """
        Post to upload a Image
        """
        profile = Profile.objects.get(user=self.request.user)

        profile.image = request.data["profileImage"]
        profile.save()

        return JsonResponse({"successful": True})
