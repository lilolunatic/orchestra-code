from modules.api import ImageUploadView
from django.urls import path

urlpatterns = [
    path('api/images', ImageUploadView.as_view())
]
