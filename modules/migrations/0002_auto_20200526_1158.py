# Generated by Django 2.2.7 on 2020-05-26 09:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modules', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='instruments',
            field=models.ManyToManyField(null=True, to='modules.Instrument'),
        ),
    ]
