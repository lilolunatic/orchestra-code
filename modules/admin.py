from django.contrib import admin

from modules.models import Instrument, Profile, Group, UserGroupRole, Score, Sheet


@admin.register(UserGroupRole)
class UserGroupRoleAdmin(admin.ModelAdmin):
    list_display = ['id', 'profile', 'group', 'role', 'list_instruments']
    search_fields = ['profile__user__username', 'group__group_name', 'role']

    @classmethod
    def list_instruments(cls, obj):
        return ", ".join([p.instrument for p in obj.instruments.all()])


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ['id', 'user']
    search_fields = ['user__username']


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ['id', 'group_name']
    search_fields = ['group_name']


@admin.register(Score)
class ScoreAdmin(admin.ModelAdmin):
    list_display = ['name', 'group']
    search_fields = ['name', 'group__group_name']


@admin.register(Sheet)
class SheetAdmin(admin.ModelAdmin):
    list_display = ['name', 'score']
    search_fields = ['name', 'score']


# Register your models here.
admin.site.register(Instrument)
