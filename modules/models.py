import io

from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.shortcuts import get_object_or_404
from rest_framework.exceptions import ValidationError


class Instrument(models.Model):
    instrument = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.instrument


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.TextField(blank=True, null=True)
    instruments = models.ManyToManyField(Instrument, blank=True)

    def __str__(self):
        return self.user.username

    @classmethod
    def addInstrument(cls, request, name):
        instrument = get_object_or_404(Instrument, name=name)
        profile = request.user.profile
        profile.instruments.add(instrument)
        profile.save()

    @classmethod
    def removeInstrument(cls, request, name):
        instrument = get_object_or_404(Instrument, name=name)
        profile = request.user.profile
        profile.instruments.delete(instrument)
        profile.save()

    def del_instrument(self, instrument: Instrument):
        for group_user in UserGroupRole.objects.filter(profile=self):
            group_user.instruments.remove(instrument)
            group_user.save()
        self.instruments.remove(instrument)


class Group(models.Model):
    group_name = models.CharField("Group name", max_length=100)

    def __str__(self):
        return self.group_name


class UserGroupRole(models.Model):

    """Describes the role a user have within a group"""

    MUSICIAN = "mu"
    MUSIC_ADMIN = "ma"
    CONDUCTOR = "co"
    available_roles = (
        (MUSICIAN, "Musician"),
        (MUSIC_ADMIN, "Music Admin"),
        (CONDUCTOR, "Conductor")
    )

    role = models.CharField("User role", max_length=2, choices=available_roles, default="mu")
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    instruments = models.ManyToManyField(Instrument, blank=True)

    def __str__(self):
        return f"{self.profile} -/- {self.group}"

    def add_instrument(self, instrument: Instrument):
        if not Instrument.objects.filter(profile=self.profile, instrument=instrument.instrument).exists():
            raise ValidationError
        self.instruments.add(instrument)
        self.save()

    class Meta:

        """additional requirement. each user can only be in a group once"""

        unique_together = ('group', 'profile')


@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


def create_profiles():
    """
    Creates a Profile for every user who does not have a UserProfile
    Can be called ones, to be sure, that every User have a profile
    :return:
    """

    user_without_profiles = User.objects.filter(profile=None)
    for user in user_without_profiles:
        Profile.objects.create(user=user)


class Score(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, null=False, blank=False)

    def __str__(self):
        return f"{self.group.group_name} - {self.name}"

    class Meta:

        """In every Group can just be different scores"""

        unique_together = ('name', 'group')


class Sheet(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    score = models.ForeignKey(Score, on_delete=models.CASCADE, null=False, blank=False)
    instrument = models.ForeignKey(Instrument, on_delete=models.CASCADE, null=False, blank=False)
    file = models.TextField(blank=True, null=True)
