import json

from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIClient
from modules.models import Profile, Score
from modules.models import Group, UserGroupRole
from modules.models import Instrument


class CreateGroupTestCase(TestCase):
    def setUp(self):
        self.user1 = User.objects.create(username="TestUser1", password=1234)

        self.user2 = User.objects.create(username="TestUser2")

        self.user3 = User.objects.create(username="TestUser3")

        Instrument.objects.create(instrument="flute")
        Instrument.objects.create(instrument="violin")
        Instrument.objects.create(instrument="piano")

    def test_post_request(self):
        client = APIClient()
        client.force_authenticate(user=self.user1)

        request = client.post('/api/group',
                              data={"groupName": "test group",
                                    "users": [{'id': self.user1.id, "role": "mu"},
                                              {"id": self.user2.id, "role": "co"},
                                              {"id": self.user3.id, "role": "ma"}],
                                    },
                              format='json')

        # check if response is correct
        self.assertEqual(request.status_code, 200)
        self.assertEqual(
            request.json.args[0].content, b'{"successful": true, "id": 1}')


class GetGroupTestCase(TestCase):
    def setUp(self):
        self.violin = Instrument.objects.create(instrument="violin")
        self.piano = Instrument.objects.create(instrument="piano")
        self.flute = Instrument.objects.create(instrument="flute")

        self.group = Group.objects.create(group_name="test-group")

        self.user1 = User.objects.create(username="TestUser1", password=1234)
        ugr1 = UserGroupRole.objects.create(
            role='co', profile=Profile.objects.get(user=self.user1), group=self.group)
        ugr1.instruments.add(self.violin, self.piano)

        self.score1 = Score.objects.create(name="score 1", group=self.group)
        self.score2 = Score.objects.create(name="score 2", group=self.group)

        self.client1 = APIClient()
        self.client1.force_authenticate(user=self.user1)

        self.user2 = User.objects.create(username="TestUser2", password=1234)
        ugr2 = UserGroupRole.objects.create(
            role='mu', profile=Profile.objects.get(user=self.user2), group=self.group)
        ugr2.instruments.add(self.piano)
        ugr2.instruments.add(self.flute)

        self.client2 = APIClient()
        self.client2.force_authenticate(user=self.user2)

        self.user3 = User.objects.create(username="TestUser3", password=1234)

    def test_get_request(self):
        request = self.client1.get('/api/group')

        self.assertEqual(request.status_code, 200)
        self.assertEqual(request.json.args[0].content,
                         b'{"groups": [{"id": %d, "name": "test-group"}]}' % self.group.id)

    def test_get_group_info(self):
        # request with a user who have a conductor role
        request = self.client1.get(f'/api/group/{self.group.id}')
        self.assertEqual(request.status_code, 200)

        json_data = request.json.args[0].content.decode(
            'utf8').replace("'", '"')
        json_data = json.loads(json_data)
        expected1 = {"groupName": self.group.group_name,
                     "members": [
                         {"id": self.user1.id, "name": self.user1.username, "role": UserGroupRole.CONDUCTOR,
                          "instruments": [self.violin.instrument, self.piano.instrument], "profileImage": None},
                         {"id": self.user2.id, "name": self.user2.username, "role": UserGroupRole.MUSICIAN,
                          "instruments": [self.piano.instrument, self.flute.instrument], "profileImage": None}
                     ],
                     "scores": [{"name": self.score1.name, "scoreId": self.score1.id},
                                {"name": self.score2.name, "scoreId": self.score2.id}]
                     }

        self.assertDictEqual(json_data, expected1)

        # request with a user who NOT have a conductor role =======================================================
        request2 = self.client2.get(f'/api/group/{self.group.id}')
        self.assertEqual(request2.status_code, 200)

        json_data = request2.json.args[0].content.decode(
            'utf8').replace("'", '"')
        json_data = json.loads(json_data)

        expected2 = {"groupName": self.group.group_name,
                     "members": [
                         {"id": self.user1.id, "name": self.user1.username, "role": UserGroupRole.CONDUCTOR,
                          "instruments": [], "profileImage": None},
                         {"id": self.user2.id, "name": self.user2.username, "role": UserGroupRole.MUSICIAN,
                          "instruments": [self.piano.instrument, self.flute.instrument], "profileImage": None}
                     ],
                     "scores": [{"name": self.score1.name, "scoreId": self.score1.id},
                                {"name": self.score2.name, "scoreId": self.score2.id}]
                     }

        self.assertDictEqual(json_data, expected2)

    def test_changeUser_request(self):
        url = f'/api/group/users/{self.group.id}'

        self.change_user_check(self.client1.post(url,
                                                 data={"users": [{"id": self.user3.id, "action": "mu"}]},
                                                 format='json'),
                               UserGroupRole.MUSICIAN)

        self.change_user_check(self.client1.post(url,
                                                 data={"users": [{"id": self.user3.id, "action": "ma"}]},
                                                 format='json'),
                               UserGroupRole.MUSIC_ADMIN)

        self.change_user_check(self.client1.post(url,
                                                 data={
                                                     "users": [{"id": self.user3.id, "action": "co"}]},
                                                 format='json'),
                               UserGroupRole.CONDUCTOR)

        request = self.client1.post(url,
                                    data={"users": [{"id": self.user3.id, "action": "del"}]},
                                    format='json')
        self.assertEqual(request.status_code, 200)
        group_user = UserGroupRole.objects.filter(profile__user=self.user3, group=self.group)
        self.assertFalse(group_user.exists())

    def change_user_check(self, response, role):
        self.assertEqual(response.status_code, 200)
        group_user = UserGroupRole.objects.filter(profile__user=self.user3, group=self.group)
        self.assertTrue(group_user.exists())
        self.assertEqual(group_user.first().role, role)


class GetGroupErrorTestCase(TestCase):
    def setUp(self) -> None:
        self.group = Group.objects.create(group_name="test-group")

        self.userError = User.objects.create(username="TestUserError", password=1234)
        UserGroupRole.objects.create(role='mu', profile=Profile.objects.get(user=self.userError), group=self.group)

        self.clientError = APIClient()
        self.clientError.force_authenticate(user=self.userError)

        self.dummyUser = User.objects.create(username="dummyUser", password=1234)

    def test_invalid_user_role(self):
        request = self.clientError.post(f'/api/group/users/{self.group.id}',
                                        data={"users": [{"id": self.dummyUser.id, "action": "add"}]},
                                        format='json')
        self.assertEqual(request.status_code, 403)


class InstrumentsApiTestCase(TestCase):

    def setUp(self) -> None:
        self.group = Group.objects.create(group_name="test-group")

        self.user = User.objects.create(username="TestUser", password=1234)
        self.group_user = UserGroupRole.objects.create(role='mu', profile=Profile.objects.get(user=self.user),
                                                       group=self.group)

        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

        flute = Instrument.objects.create(instrument="flute")
        violin = Instrument.objects.create(instrument="violin")
        piano = Instrument.objects.create(instrument="piano")

        self.user.profile.instruments.add(flute)
        self.user.profile.instruments.add(violin)
        self.user.profile.instruments.add(piano)

    def test_post(self):
        request = self.client.post('/api/group/instruments',
                                   data={"id": self.group.id, "name": ["flute", "violin"]},
                                   format='json')

        self.assertEqual(request.status_code, 200)
        self.assertListEqual(list(Instrument.objects.filter(usergrouprole=self.group_user)),
                             list(Instrument.objects.filter(instrument__in=["flute", "violin"])))

        request = self.client.post('/api/group/instruments',
                                   data={"id": self.group.id, "name": ["flute"]},
                                   format='json')
        self.assertListEqual(list(Instrument.objects.filter(usergrouprole=self.group_user)),
                             list(Instrument.objects.filter(instrument__in=["flute"])))
        self.assertEqual(request.status_code, 200)

        request = self.client.post('/api/group/instruments',
                                   data={"id": self.group.id, "name": []},
                                   format='json')
        self.assertListEqual(list(Instrument.objects.filter(usergrouprole=self.group_user)),
                             list(Instrument.objects.filter(instrument__in=[])))
        self.assertEqual(request.status_code, 200)
