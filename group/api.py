from django.http import JsonResponse
from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied
from rest_framework.views import APIView

from group.group_helpers import ConductorGroupInformation, MusicianGroupInformation, MusicAdminGroupInformation
from modules.models import Profile, Instrument
from modules.models import Group, UserGroupRole


class InstrumentsApi(APIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    http_method_names = ['head', 'post']

    def post(self, request):
        data = self.request.data

        instrument_names = self.request.data["name"]
        new_player_instruments = set(Instrument.objects.filter(instrument__in=instrument_names))

        current_player_instruments = set(Instrument.objects.filter(usergrouprole__profile=self.request.user.profile,
                                                                   usergrouprole__group_id=data["id"]))

        group_user = UserGroupRole.objects.get(group=data["id"], profile=self.request.user.profile)

        instruments_to_add = new_player_instruments.difference(current_player_instruments)
        for instrument in instruments_to_add:
            group_user.add_instrument(instrument)

        instruments_to_remove = current_player_instruments.difference(new_player_instruments)
        for instrument in instruments_to_remove:
            group_user.instruments.remove(instrument)

        return JsonResponse({"successful": True})


class GroupUserViewSet(APIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    http_method_names = ['head', 'post']

    def post(self, request, group_id):

        """
        post request to add, remove or change a roll a user from group
        Backend get data in the following json format:
        {"users": [{“id”: 1, “action”: "del"/ "ma"/ "mu"/ "co"}, ...]}
        """

        data = self.request.data
        group = Group.objects.get(id=group_id)

        if self.get_user_group_role(self.request.user.profile, group).role == UserGroupRole.MUSICIAN:
            raise PermissionDenied

        for user in data["users"]:
            user_profile = Profile.objects.get(user_id=user["id"])
            if user["action"] == "del":
                user_group_role = self.get_user_group_role(user_profile, group)
                user_group_role.delete()
            elif user["action"] in ["mu", "ma", "co"]:
                user_group_role = self.get_user_group_role(user_profile, group)
                user_group_role.role = user["action"]
                user_group_role.save()
            else:
                raise ValueError(f'Invalid Value for "action" key in json request. \n'
                                 f'Allowed are: "del", "ma", "mu", "co"')

        return JsonResponse({"successful": True})

    @classmethod
    def get_user_group_role(cls, profile: Profile, group: Group):
        user_group_role = UserGroupRole.objects.filter(profile=profile, group=group)

        if user_group_role.exists():
            # combination profile and group is unique
            return user_group_role.first()
        else:
            return UserGroupRole.objects.create(group=group, profile=profile)

class CreateGroupViewSet(APIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    http_method_names = ['head', 'post', 'get']

    def post(self, request):
        """
        post request to create a new group with member in it
        get data in the following json format: {"users": [{"id": 1, "role": "mu"}, ...],
                                                "groupName": "name of group"}
        :param request:
        :return:
        """

        data = self.request.data

        group = Group.objects.create(group_name=data["groupName"])

        for user in data["users"]:
            user_profile = Profile.objects.get(user__id=user["id"])
            if user_profile.user == self.request.user:
                continue
            UserGroupRole.objects.create(role=user["role"], profile=user_profile, group=group)

        # add current logged in user to group as Conductor
        UserGroupRole.objects.create(role=UserGroupRole.CONDUCTOR, profile=self.request.user.profile, group=group)

        return JsonResponse({"successful": True, "id": group.id})

    def get(self, request):
        """
        get all groups where the logged in user is
        return json in following format: {'groups': [{"id": 1, "name": "group name"}, ...]}
        :param request:
        :return:
        """

        groups = Group.objects.filter(usergrouprole__profile__user=self.request.user).values('id', 'group_name')

        # change key 'group_name' to 'name'
        for group in groups:
            group["name"] = group.pop("group_name")

        return JsonResponse({'groups': list(groups)})


class GetGroupViewSet(APIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    http_method_names = ['head', 'get']

    def get(self, request, group_id):
        """
        get the data from a specific group.
        return json in the following format: {"groupName": "name",
                                              "members": {"id": 1, "role": "co", "instruments": ["piano", "flute"]}}
        get the instruments from a member, only if the logged in user has the conductor role for the given group
        :param request:
        :param group_id:
        :return:
        """

        user_group_role = UserGroupRole.objects.get(group__id=group_id, profile__user=self.request.user)

        if user_group_role.role == UserGroupRole.CONDUCTOR:
            return ConductorGroupInformation(group_id, request).get_json_response()
        elif user_group_role == UserGroupRole.MUSIC_ADMIN:
            return MusicAdminGroupInformation(group_id, request).get_json_response()
        else:
            return MusicianGroupInformation(group_id, request).get_json_response()
