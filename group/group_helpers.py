import abc

from django.http import JsonResponse
from rest_framework.request import Request

from modules.models import Group, Profile, Score, Instrument


class GroupInformation(metaclass=abc.ABCMeta):

    def __init__(self, group_id: int, request: Request):
        self.group = Group.objects.get(id=group_id)
        self.request = request

    def get_json_response(self):
        return JsonResponse({'groupName': self.group.group_name, 'members': self._get_member_list(), "scores": self._get_scores_list()})

    @classmethod
    def _get_profile_images(cls, group: Group):
        profiles = Profile.objects.filter(usergrouprole__group=group).distinct()
        images = [profile.image for profile in profiles]
        return images

    @classmethod
    def _transform_member_dict(cls, member: dict):
        if "usergrouprole__instruments__instrument" in member:
            instruments = [member["usergrouprole__instruments__instrument"]]
        else:
            instruments = []

        return {'id': member["user__id"], 'name': member["user__username"], "role": member["usergrouprole__role"],
                "instruments": instruments, 'profileImage': member["image"]}

    @abc.abstractmethod
    def _get_member_list(self):
        pass

    def _get_scores_list(self):
        scores = Score.objects.filter(group_id=self.group.id).values('name', 'id')
        scores = [{'name': score["name"], 'scoreId': score["id"]} for score in scores]

        return scores


class ConductorGroupInformation(GroupInformation):

    def _get_member_list(self):

        members = list(Profile.objects.filter(usergrouprole__group=self.group)
                       .values("user__username", "user__id", "usergrouprole__role",
                               "usergrouprole__instruments__instrument", "image"))
        # get for a user with multiple instruments in a group, multiple list entries

        # in the next steps this multiple lst entries will be combined in one entry (also change the name from the keys)
        members.sort(key=lambda x: x['user__id'])

        member_list = {}
        user_ids = []
        for member in members:
            # change key names
            member = self._transform_member_dict(member)

            if member["id"] in user_ids:
                member_list[member['id']]["instruments"].append(member["instruments"][0])
            else:
                member_list[member['id']] = member
                user_ids.append(member['id'])

        return list(member_list.values())


class MusicianGroupInformation(GroupInformation):

    def _get_member_list(self):
        members = list(Profile.objects.filter(usergrouprole__group=self.group)
                       .values("user__username", "user__id", "usergrouprole__role", "image"))

        members = [self._transform_member_dict(member) for member in members]

        # current user where is logged in
        user_instruments = Instrument.objects.filter(
            usergrouprole__group=self.group,
            usergrouprole__profile=self.request.user.profile,
        ).values_list("instrument")

        for member in members:
            if member["id"] == self.request.user.id:
                member["instruments"] = [instrument[0] for instrument in user_instruments]
                break

        return members


class MusicAdminGroupInformation(MusicianGroupInformation):
    """
    MusicianGroupInformation and MusicAdminGroupInformation, do exactly the same.
    two classes were implemented for later extension
    """

    pass
