from django.urls import path

from group.api import CreateGroupViewSet, GetGroupViewSet, GroupUserViewSet, InstrumentsApi

urlpatterns = [
    path('api/group', CreateGroupViewSet.as_view(), name="group"),
    path('api/group/<int:group_id>', GetGroupViewSet.as_view(), name="group"),
    path('api/group/users/<int:group_id>', GroupUserViewSet.as_view(), name="group"),
    path('api/group/instruments', InstrumentsApi.as_view(), name="group"),
]
